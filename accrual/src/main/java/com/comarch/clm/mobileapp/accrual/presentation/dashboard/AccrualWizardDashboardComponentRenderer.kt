package com.comarch.clm.mobileapp.accrual.presentation.dashboard

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.comarch.clm.mobileapp.accrual.presentation.dashboard.AccrualWizardDashboardComponentController.Companion.ACCRUAL_DASHBOARD_ROUTE
import com.comarch.clm.mobileapp.core.Architecture.Router
import com.comarch.clm.mobileapp.core.presentation.dashboard.DashboardComponentContract.DashboardComponentRenderer
import com.comarch.clm.mobileapp.core.presentation.dashboard.DashboardComponentContract.DashboardRendererInitializationException
import com.comarch.clm.mobileapp.core.presentation.dashboard.DashboardComponentContract.UsesRouter
import kotlinx.android.synthetic.main.view_acrual_wizard_dashboard.view.mainLayout
import kotlinx.android.synthetic.main.view_acrual_wizard_dashboard.view.registerTransaction

class AccrualWizardDashboardComponentRenderer(val view: View,
    val router: Router) : DashboardComponentRenderer<Boolean> {

  init {
    view.registerTransaction.setOnClickListener {
      router.nextScreen(ACCRUAL_DASHBOARD_ROUTE)
    }
  }

  override fun render(state: Boolean?) {
    if (state==true) {
      view.mainLayout.visibility = VISIBLE
    } else {
      view.mainLayout.visibility = GONE
    }
  }


  class AccrualWizardDashboardRendererBuilder : DashboardComponentRenderer.Builder<Boolean>, UsesRouter {

    override var router: Router? = null

    override fun build(view: View): DashboardComponentRenderer<Boolean> {
      return AccrualWizardDashboardComponentRenderer(view,
          router ?: throw DashboardRendererInitializationException)
    }
  }


}