package com.comarch.clm.mobileapp.accrual.presentation.invoice

import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle.Event.ON_CREATE
import androidx.lifecycle.Lifecycle.Event.ON_DESTROY
import androidx.lifecycle.OnLifecycleEvent
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddInvoiceViewState
import com.comarch.clm.mobileapp.accrual.AccrualContract.Companion.CHOOSE_PDF_REQUEST_CODE
import com.comarch.clm.mobileapp.accrual.AccrualContract.Companion.CHOOSE_PHOTO_REQUEST_CODE
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.accrual.R.string
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.CLMCallback
import com.comarch.clm.mobileapp.core.presentation.BaseFragment
import com.comarch.clm.mobileapp.core.presentation.image.ImageRenderer
import com.comarch.clm.mobileapp.core.presentation.toolbar.ToolbarContract.State
import com.comarch.clm.mobileapp.core.util.ActivityWrapper
import com.comarch.clm.mobileapp.core.util.components.CLMCombinedValidator
import com.comarch.clm.mobileapp.core.util.components.CLMCoordinatorLayout
import com.comarch.clm.mobileapp.core.util.components.CLMFieldRequiredValidator
import com.comarch.clm.mobileapp.core.util.components.CLMTextLengthValidator
import com.comarch.clm.mobileapp.core.util.hideKeyboard
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.jakewharton.rxbinding2.view.RxView
import com.theartofdev.edmodo.cropper.CropImage
import io.reactivex.Observable
import kotlinx.android.synthetic.main.screen_add_invoice.view.addUploadFileButton
import kotlinx.android.synthetic.main.screen_add_invoice.view.attachmentImage
import kotlinx.android.synthetic.main.screen_add_invoice.view.attachmentLabel
import kotlinx.android.synthetic.main.screen_add_invoice.view.attachmentLayout
import kotlinx.android.synthetic.main.screen_add_invoice.view.informationMaxSize
import kotlinx.android.synthetic.main.screen_add_invoice.view.loader
import kotlinx.android.synthetic.main.screen_add_invoice.view.numberInvoiceEditText
import kotlinx.android.synthetic.main.screen_add_invoice.view.removeAttachment
import kotlinx.android.synthetic.main.screen_add_invoice.view.toolbar
import kotlinx.android.synthetic.main.screen_add_invoice.view.viewFileBottomPanel
import kotlinx.android.synthetic.main.view_action_button.view.buttonAction
import kotlinx.android.synthetic.main.view_error_information.view.labelInformation
import kotlinx.android.synthetic.main.view_file_choose_panel.view.chooseFilesLayout
import kotlinx.android.synthetic.main.view_file_choose_panel.view.chooseImageGalleryLayout
import kotlinx.android.synthetic.main.view_file_choose_panel.view.makePhotoLayout
import java.io.File

class AddInvoiceScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CLMCoordinatorLayout(
    context,
    attrs,
    defStyleAttr
), AccrualContract.AddInvoiceView, CLMCallback {

  lateinit var callbacksHandler: Architecture.CLMCallbacksHandler
  lateinit var imageRenderer: ImageRenderer

  private var bottomSheetBehavior: BottomSheetBehavior<ViewGroup>? = null


  override lateinit var presenter: AccrualContract.AddInvoicePresenter


  @OnLifecycleEvent(ON_DESTROY)
  fun onDestroy() {
    callbacksHandler.removeCallback(this)
  }

  @OnLifecycleEvent(ON_CREATE)
  fun onCreate() {
    callbacksHandler.addCallback(this)
  }

  override fun render(state: AddInvoiceViewState) {
    if (!state.invoiceNumber.isNullOrEmpty() && state.invoiceNumber != numberInvoiceEditText.editInputText.text.toString()) {
      numberInvoiceEditText.text = state.invoiceNumber
    }
    renderInformationSize(state)
    renderFile(state)
    renderFile(state)
    renderView(state)
    if (state.isFileTooBig) {
      presenter.goToBigToFileDialog()
      return
    }

  }

  private fun renderView(
      state: AddInvoiceViewState) {
    val isEnabledButton = state.isAttachmentAttached && !numberInvoiceEditText.text.isEmpty()
    buttonAction.setStateEnabled(isEnabledButton)
    if (state.isAttachmentAttached) {
      informationMaxSize.visibility = GONE
      addUploadFileButton.visibility = GONE
      attachmentLayout.visibility = VISIBLE
      viewFileBottomPanel.visibility = GONE
      bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
      viewFileBottomPanel.visibility = View.GONE
    } else {
      addUploadFileButton.visibility = VISIBLE
      attachmentLayout.visibility = GONE
      viewFileBottomPanel.visibility = VISIBLE
    }
  }

  private fun renderFile(
      state: AddInvoiceViewState) {
    if (state.invoiceUri != null) {

      val file = File(state.invoiceUri)
      val ls = file.length().toFloat() / (1024 * 1024)
      if (file.extension != "pdf" && file.extension.isNotBlank()) {
        imageRenderer.renderLocalImage(attachmentImage, state.invoiceUri)
        attachmentImage.visibility = VISIBLE
        attachmentLabel.text = context.getString(string.labels_cma_accrual_attachment)
      } else {
        attachmentLabel.text = file.nameWithoutExtension + "." + file.extension
        attachmentImage.visibility = GONE
      }
    }
  }

  private fun renderInformationSize(
      state: AddInvoiceViewState) {
    if (state.maxSizeFile > 0) {
      informationMaxSize.visibility = VISIBLE
      informationMaxSize.labelInformation.text = context.getString(
          string.labels_cma_accrual_attachment_sizeLimit, state.maxSizeFile)
    } else {
      informationMaxSize.visibility = GONE
    }
  }

  override fun invoiceNoChanged(): Observable<String> {
    return CLMCombinedValidator(
        CLMFieldRequiredValidator(),
        CLMTextLengthValidator(maxLength = 50, errorMessage = context.getString(
            string.labels_cma_accrual_add_invoice_number_maxLength, 50)))
        .attachTo(
            numberInvoiceEditText).map {
          numberInvoiceEditText.editInputText.text.toString().trim()
        }
//     RxTextView.textChanges(numberInvoiceEditText.editInputText)
//        .map {
//          it.toString().trim()
//        }
  }

  override fun onPressedAddFile(): Observable<Any> =
      RxView.clicks(addUploadFileButton).map {
        injector(context).instance<ActivityWrapper>().activity?.hideKeyboard()
        viewFileBottomPanel.visibility = View.VISIBLE
        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
      }


  override fun onPressedChooseGallery(): Observable<Any> {
    return RxView.clicks(chooseImageGalleryLayout)
  }

  override fun onPressedChooseFile(): Observable<Any> {
    return RxView.clicks(chooseFilesLayout)
  }

  override fun onPressedMakePhoto(): Observable<Any> {
    return RxView.clicks(makePhotoLayout)
  }

  override fun onPressedNext(): Observable<String> {
    return RxView.clicks(buttonAction).map {
      numberInvoiceEditText.text
    }
  }

  override fun onPressedRemoveInvoiceFile(): Observable<Any> {
    return RxView.clicks(removeAttachment)
  }

  override fun showChooseFile() {
    bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
    viewFileBottomPanel.visibility = View.VISIBLE
  }

  override fun init() {
    initToolbar()
    initBottomSheet()
    initLoader()

  }

  private fun initLoader() {
    loader.max = 3
    loader.progress = 1
    loader.progress = 2
  }


  override fun inject(fragment: BaseFragment) {
    presenter =
        injector(context).with(Pair<AccrualContract.AddInvoiceView, Fragment>(this, fragment))
            .instance()
    callbacksHandler = injector(context).instance<Architecture.CLMCallbacksHandler>()
    callbacksHandler.addCallback(this)
    imageRenderer = injector(context).with(fragment as Fragment)
        .instance()
  }

  private fun initToolbar() {
    toolbar.setState(State.DEFAULT)
    toolbar.setTitle(context.getString(string.labels_cma_accrual_add_invoice))
  }

  override fun activityResult(requestCode: Int, resultCode: Int, data: Intent?) {

    super.activityResult(requestCode, resultCode, data)
    if (requestCode == CHOOSE_PHOTO_REQUEST_CODE) {
      presenter.openCrop(data)
    }

    if (requestCode == CHOOSE_PDF_REQUEST_CODE) {
      data?.data?.let { presenter.saveInvoiceFile(it, "application/pdf") }
    }
    if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
      if (data?.extras?.get(CropImage.CROP_IMAGE_EXTRA_RESULT) != null) {

      }
    }
  }


  private fun initBottomSheet() {
    bottomSheetBehavior = BottomSheetBehavior.from(viewFileBottomPanel as LinearLayout)
  }

  companion object {
    val ENTRY = Architecture.Navigator.NavigationPoint(R.layout.screen_add_invoice)
  }
}

