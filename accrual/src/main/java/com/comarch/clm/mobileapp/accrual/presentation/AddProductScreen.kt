package com.comarch.clm.mobileapp.accrual.presentation

import android.content.Context
import android.util.AttributeSet
import androidx.fragment.app.Fragment
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddProductViewState
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.BaseFragment
import com.comarch.clm.mobileapp.core.presentation.toolbar.ToolbarContract.State
import com.comarch.clm.mobileapp.core.util.components.CLMConstraintLayout
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.screen_add_product.view.eanEditText
import kotlinx.android.synthetic.main.screen_add_product.view.toolbar
import kotlinx.android.synthetic.main.view_action_button.view.buttonAction

class AddProductScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CLMConstraintLayout(
    context,
    attrs,
    defStyleAttr
), AccrualContract.AddProductView {

  override lateinit var presenter: AccrualContract.AddProductPresenter

  override fun render(state: AddProductViewState) {
  }

  override fun onPressedDone(): Observable<String> {
    return RxView.clicks(buttonAction).map {
      eanEditText.text }

  }

  override fun init() {
    initToolbar()
  }

  override fun inject(fragment: BaseFragment) {
    presenter =
        injector(context).with(Pair<AccrualContract.AddProductView, Fragment>(this, fragment))
            .instance()
    eanEditText.setMaxLine(1 )
  }

  private fun initToolbar() {
    toolbar.setState(State.DEFAULT)
    toolbar.onFirstRightIconClick = {
      injector(context).instance<Architecture.Router>().previousScreen()
    }

    toolbar.setFirstRightCustomIcon((
        context.getDrawable(com.comarch.clm.mobileapp.core.R.drawable.close_button_3)))
  }

//  private fun initTitle() {
//    val mListener = AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
//      if (productListTitle.height + verticalOffset <= 1) {
//        productListTitle.animate().alpha(0f)
//        toolbar.findViewById<CLMLabel>(R.id.toolbar_title).animate().alpha(1f)
//        toolbar.setTitle(R.string.labels_cma_accrual_productList)
//      } else {
//        toolbar.findViewById<CLMLabel>(R.id.toolbar_title).animate().alpha(0f)
//        toolbar.setTitle("")
//        productListTitle.animate().alpha(1f)
//      }
//    }
//    appbar.addOnOffsetChangedListener(mListener)
//  }


  companion object {
    val ENTRY = Architecture.Navigator.NavigationPoint(R.layout.screen_add_product)
  }


}