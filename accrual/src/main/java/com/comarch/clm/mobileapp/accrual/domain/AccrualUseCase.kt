package com.comarch.clm.mobileapp.accrual.domain

import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualRequest
import com.comarch.clm.mobileapp.accrual.data.model.AccrualRequestProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponse
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct.Companion.FIELD_CODE
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct.Companion.FIELD_RESTRICTED
import com.comarch.clm.mobileapp.accrual.data.model.InvoiceFile
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualProductImpl
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualWizardProductImpl
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualWizardProductItemImpl
import com.comarch.clm.mobileapp.core.ApplicationContract
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.DataSynchronizationContract
import com.comarch.clm.mobileapp.core.data.model.Parameter
import com.comarch.clm.mobileapp.core.data.repository.filter.PredicateFactory
import com.comarch.clm.mobileapp.core.domain.UseCase
import com.comarch.clm.mobileapp.core.util.ClmLogger
import com.comarch.clm.mobileapp.core.util.ClmOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.realm.RealmList
import okhttp3.MultipartBody.Part
import retrofit2.HttpException
import java.util.Date

class AccrualUseCase(
    private val errorHandler: Architecture.ErrorHandler,
    schedulerApplier: Architecture.SchedulerApplier,
    applicationState: ApplicationContract.ApplicationState,
    private val repository: AccrualContract.AccrualRepository,
    private val synchronizationUseCase: DataSynchronizationContract.DataSynchronizationManager,
    private val predicateFactory: PredicateFactory
) : UseCase(schedulerApplier, applicationState), AccrualContract.AccrualUseCase {

  override fun getParameter(code: String): Observable<ClmOptional<Parameter?>> {
    val localParameter = repository.getObservable(
        Parameter::class.java,
        fieldName = "code",
        fieldValue = code
    )

    val remoteParameter = repository.fetchParameter(code)
        .compose(schedulerApplier.changeToForegroundSingle())
        .map {
          repository.save(it.apply { this.code = code })
          it
        }
        .map {
          ClmOptional.of(it)
        }
        .toObservable()
        .onErrorResumeNext { t: Throwable ->
          ClmLogger.log("error updating parameter " + t.message)
          Observable.just(ClmOptional.of<Parameter?>(null))
        }

    return Observable.merge(localParameter, remoteParameter)
  }

  override fun getAccrualWizardProductList(): List<AccrualWizardProduct> {
    return repository.getAccrualWizardProductList()
  }

  override fun getAccrualWizardProducts(
      isFoundProduct: Boolean): Observable<List<AccrualWizardProduct>> {
    val predicate = if (isFoundProduct) {
//      predicateFactory.not(predicateFactory.equal("code",""))
      predicateFactory.and(
          predicateFactory.not(predicateFactory.equal(FIELD_CODE, "")),
          predicateFactory.equal(FIELD_RESTRICTED, false)
      )
    } else {
      null
    }
    return repository.getAccrualWizardProducts(predicate)
  }

  override fun addWizardProduct(externalCode: String): Completable {
    return fetchWizardProduct(externalCode)
        .compose(errorHandler.handleSingleError())
        .flatMapCompletable {
          updateWizardProducts(it)
        }
  }

  override fun updateWizardProduct(externalCode: String, isUpdateName: Boolean): Completable {
    return fetchProductWithError(externalCode).compose(synchronizationUseCase.checkIfNeededSingle(
        AccrualWizardProduct::class.java)).flatMapCompletable { product ->
      val externalCode = product.externalCode
      val code = product.code

      val wizardProduct = repository.getCopyByCode(AccrualWizardProduct::class.java, code)
      if (wizardProduct != null) {
        overrideProduct(externalCode, wizardProduct, product, isUpdateName)
        Completable.fromCallable {
          repository.save(wizardProduct)
        }
      } else {
        Completable.complete()
      }
    }
  }

  override fun deleteWizardProduct(id: String): Completable {
    return repository.deleteWizardProduct(id)
  }

  override fun buildRequest(product: List<AccrualWizardProduct>,
      fileAttachment: InvoiceFile?): AccrualRequest {
    val accrualProducts = product.map {
      AccrualRequestProduct(code = it.code, quantity = it.items.size)
    }

    return AccrualRequest(comment = null, products = accrualProducts, fileId = null,
        trnNo = fileAttachment?.invoiceNo)
  }

  override fun makeAccrual(request: AccrualRequest, simulate: Boolean,
      filePart: Part?): Single<AccrualResponse> {
    return if (filePart != null) {
      repository.uploadInvoice(filePart)
          .flatMap {
            val newRequest = request
            newRequest.fileId = it
            makeAccrualWithoutInvoice(newRequest, simulate)
          }
    } else {
      makeAccrualWithoutInvoice(request, simulate)
    }


  }

  private fun makeAccrualWithoutInvoice(
      request: AccrualRequest,
      simulate: Boolean): Single<AccrualResponse> {
    return repository.makeSale(request, simulate)
        .compose(schedulerApplier.changeToForegroundSingle())
        .compose(schedulerApplier.startOnBackgroundSingle())
        .compose(errorHandler.handleSingleError()).doOnSuccess {
          if (!simulate) {
            updateAfterWizardCompleted()
          }
        }
  }

  private fun updateAfterWizardCompleted() {
    repository.deleteWizardProductsAndInvoiceFile()
//        .andThen(updatedUseCase.updateAfterAccrual())
  }

  fun updateWizardProducts(product: AccrualProduct): Completable {
    val externalCode = product.externalCode
    val code = product.code

    var wizardProduct = repository.getCopyByCode(AccrualWizardProduct::class.java, code)
    if (wizardProduct != null) {
      overrideProduct(externalCode, wizardProduct, product, false)
    } else {
      wizardProduct = createNewProduct(externalCode, wizardProduct, code, product)
    }
    return Completable.fromCallable {
      repository.save(wizardProduct)
    }
  }

  private fun overrideProduct(externalCode: String,
      wizardProduct: AccrualWizardProduct,
      product: AccrualProduct, isUpdateName: Boolean) {
    val itemProduct = AccrualWizardProductItemImpl(code = externalCode, addedDate = Date())
    wizardProduct.addedDate = Date()
    wizardProduct.points = product.points
    wizardProduct.name = product.name
    if (!isUpdateName) {
      val mutableList = wizardProduct.items.toMutableList()
      mutableList.add(itemProduct)
      wizardProduct.items = mutableList.toList()
    }

  }

  private fun createNewProduct(externalCode: String,
      wizardProduct: AccrualWizardProduct?,
      code: String,
      product: AccrualProduct): AccrualWizardProduct {
    var wizardProduct1 = wizardProduct
    val itemProduct = AccrualWizardProductItemImpl(code = externalCode, addedDate = Date())
    wizardProduct1 = AccrualWizardProductImpl(
        code = code,
        name = product.name,
        restricted = product.resticted,
        addedDate = Date(),
        _points = (product as AccrualProductImpl)._points,
        _items = RealmList(itemProduct)
    )
    return wizardProduct1
  }


  override fun fetchWizardProduct(externalCode: String): Single<AccrualProduct> {
    return fetchProductWithError(externalCode).flatMap { product ->
      val requestProduct = if (product.code != "") {
        val currentProduct = repository.getByCode(AccrualWizardProduct::class.java, product.code)
        val quantity = (currentProduct?.items?.size ?: 0) + 1
        AccrualRequestProduct(code = product.code, quantity = quantity)
      } else {
        AccrualRequestProduct(externalCode = product.externalCode, quantity = 1)
      }
      val accrualRequest = AccrualRequest(listOf(requestProduct), null, null, null)
      repository.makeSale(accrualRequest, simulate = true).flatMap { response ->
        if (response.transactionPoints?.points != null) {
          product.points = response.transactionPoints?.points!!
        }
        Single.just(product)
      }
    }
  }

  override fun uploadInvoice(avatar: Part): Single<String> {
    return repository.uploadInvoice(avatar)
        .compose(errorHandler.handleSingleError())
        .compose(schedulerApplier.changeToForegroundSingle())
        .compose(schedulerApplier.startOnBackgroundSingle())
  }

  override fun getLocalObservableFileInvoice(): Observable<ClmOptional<InvoiceFile?>> {
    return repository.getLocalFileInvoice()
  }

  override fun getLocalFileInvoice(): InvoiceFile? {
    return repository.get(InvoiceFile::class.java)
  }

  override fun saveInvoiceFile(path: String?, number: String?, type: String?): Completable {
    return repository.saveInvoiceFile(path, number, type)
  }

  override fun deleteInvoiceFile(): Completable = repository.deleteInvoiceFile()

  private fun fetchProductWithError(
      externalCode: String): Single<AccrualProduct> {
    return repository.fetchProduct(externalCode).onErrorReturn {
      if (it is HttpException) {
        if (it.code() == 404) {
          val product = AccrualProductImpl(resticted = true, externalCode = externalCode)
          product
        } else {
          throw it
        }
      } else {
        throw it
      }
    }
  }


}