package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualWizardProductImpl
import com.comarch.clm.mobileapp.core.data.model.ModelHandler
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class AccrualWizardProductHandler : ModelHandler<AccrualWizardProduct> {

  override fun instantiate(): AccrualWizardProduct = AccrualWizardProductImpl()

  override fun getClassMappings(): Map<Class<*>, Class<*>> {
    return mapOf(
        AccrualWizardProduct::class.java to AccrualWizardProductImpl::class.java
    )
  }

  @FromJson
  fun toInterface(modelImpl: AccrualWizardProductImpl): AccrualWizardProduct {
    return modelImpl
  }

  @ToJson
  fun toImplementation(modelIf: AccrualWizardProduct): AccrualWizardProductImpl {
    return modelIf as AccrualWizardProductImpl
  }
}