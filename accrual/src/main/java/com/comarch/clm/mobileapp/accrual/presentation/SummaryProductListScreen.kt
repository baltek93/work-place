package com.comarch.clm.mobileapp.accrual.presentation

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.fragment.app.Fragment
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.SummaryProductListPresenter
import com.comarch.clm.mobileapp.accrual.AccrualContract.SummaryProductListViewState
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.accrual.R.layout
import com.comarch.clm.mobileapp.accrual.R.string
import com.comarch.clm.mobileapp.accrual.presentation.PointsTypesView.PointsTypeObject
import com.comarch.clm.mobileapp.accrual.presentation.ProductListScreen.Companion.createStringSummary
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.CLMListView.ViewType
import com.comarch.clm.mobileapp.core.presentation.BaseFragment
import com.comarch.clm.mobileapp.core.presentation.image.ImageRenderer
import com.comarch.clm.mobileapp.core.util.components.CLMButton
import com.comarch.clm.mobileapp.core.util.components.CLMConstraintLayout
import com.comarch.clm.mobileapp.core.util.components.resources.CLMResourcesResolver
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.item_attachment_file.view.attachmentCardView
import kotlinx.android.synthetic.main.item_attachment_file.view.attachmentImage
import kotlinx.android.synthetic.main.item_attachment_file.view.attachmentLabel
import kotlinx.android.synthetic.main.item_attachment_file.view.editAttachmentButton
import kotlinx.android.synthetic.main.item_attachment_file.view.invoiceNoValue
import kotlinx.android.synthetic.main.product_item.view.codeLabel
import kotlinx.android.synthetic.main.product_item.view.nameLabel
import kotlinx.android.synthetic.main.product_item.view.pointTypesViewTest
import kotlinx.android.synthetic.main.product_item.view.quantityValue
import kotlinx.android.synthetic.main.product_item.view.statusLabel
import kotlinx.android.synthetic.main.product_item.view.summaryIconError
import kotlinx.android.synthetic.main.product_item.view.summaryLabelError
import kotlinx.android.synthetic.main.screen_summary_product_list.view.extraPointTypesView
import kotlinx.android.synthetic.main.screen_summary_product_list.view.extraPointsLabel
import kotlinx.android.synthetic.main.screen_summary_product_list.view.loader
import kotlinx.android.synthetic.main.screen_summary_product_list.view.pointTypesView
import kotlinx.android.synthetic.main.screen_summary_product_list.view.productListView
import kotlinx.android.synthetic.main.screen_summary_product_list.view.progressButtonAction
import kotlinx.android.synthetic.main.screen_summary_product_list.view.totalLayoutError
import kotlinx.android.synthetic.main.screen_summary_product_list.view.totalPointsLabel
import java.io.File

class SummaryProductListScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CLMConstraintLayout(
    context,
    attrs,
    defStyleAttr
), AccrualContract.SummaryProductListView {

  override lateinit var presenter: SummaryProductListPresenter
  lateinit var imageRenderer: ImageRenderer
  lateinit var clmResourcesResolver: CLMResourcesResolver

  override fun onPressedRegister(): Observable<Any> {
    return RxView.clicks(progressButtonAction.findViewById<CLMButton>(R.id.actionButton))
  }

  override fun render(state: SummaryProductListViewState) {
    if (progressButtonAction.stateButton != state.stateButton) {
      progressButtonAction.setState(state.stateButton)
    }
    if (state.isAddInvoice) {
      setLoader(3)
    } else {
      setLoader(2)
    }

    productListView.render(object : Architecture.CLMListView.Renderable {
      override val size: Int = if (state.isAddInvoice) state.productList.size + 1 else state.productList.size

      override fun bindView(view: View, position: Int) {
        if (getViewTypeForPos(position) == ViewType.ITEM_TYPE) {
          renderProductItem(state, position, view)
        } else if (getViewTypeForPos(position) == ViewType.FOOTER_TYPE) {

          view.editAttachmentButton.setOnClickListener {
            presenter.onEditPressed()
          }
          view.invoiceNoValue.text = state.fileAttachment?.invoiceNo ?: ""
          view.invoiceNoValue.text = state.fileAttachment?.invoiceNo ?: ""

          if (state.fileAttachment?.path != null) {
            val file = File(state.fileAttachment.path)
            if (file.extension != "pdf" && file.extension.isNotBlank()) {
              imageRenderer.renderLocalImage(view.attachmentImage, state.fileAttachment.path!!)
              view.attachmentImage.visibility = View.VISIBLE
              view.attachmentLabel.text = context.getString(string.labels_cma_accrual_attachment)
            } else {
              view.attachmentLabel.text = file.nameWithoutExtension + "." + file.extension
              view.attachmentImage.visibility = View.GONE
            }
            view.attachmentCardView.setCardBackgroundColor(
                ErrorInformationView.getTransparentColorForPointTypeBackground(
                    clmResourcesResolver.resolveColor(string.styles_color_shadow)))
          }
        }
      }

      override fun getViewTypeForPos(position: Int): Int {
        return if (state.isAddInvoice && position == (size - 1)) {
          ViewType.FOOTER_TYPE
        } else {
          ViewType.ITEM_TYPE
        }
      }
    })
    val items = state.productList.size

    if (state.totalPoints.firstOrNull
        { it.points > 0 } != null) {
      totalLayoutError.visibility = View.GONE
      pointTypesView.visibility = View.VISIBLE
      pointTypesView.render(PointsTypeObject(pointsList = state.totalPoints, isGrid = true))
    } else {
      totalLayoutError.visibility = View.VISIBLE
      pointTypesView.visibility = View.GONE
    }
    if (state.extraPoints.firstOrNull
        { it.points > 0 } != null) {
      extraPointsLabel.visibility = View.VISIBLE
      extraPointTypesView.visibility = View.VISIBLE
      extraPointTypesView.render(PointsTypeObject(pointsList = state.extraPoints, isGrid = true))
    } else {
      extraPointsLabel.visibility = View.GONE
      extraPointTypesView.visibility = View.GONE
    }

    totalPointsLabel.text = context.getString(string.labels_cma_accrual_wizardSummary_totalPoints,
        items)
  }

  private fun renderProductItem(
      state: SummaryProductListViewState,
      position: Int, view: View) {
    val product = state.productList[position]

    view.quantityValue.text = product.items.size.toString()
    view.nameLabel.text = product.name
    view.codeLabel.visibility = GONE
    view.statusLabel.visibility = GONE

    if (product.points.isNotEmpty()) {
      view.pointTypesViewTest.render(
          PointsTypeObject(pointsList = product.points, isGrid = true,
              labelText = createStringSummary(product.points, product.items.size)))
    } else {
      view.summaryIconError.visibility = VISIBLE
      view.summaryLabelError.visibility = VISIBLE
    }
  }


  override fun init() {
    initList()
    setLoader(2)
  }

  private fun setLoader(maxValue: Int) {
    loader.max = maxValue
    loader.progress = 1
    loader.progress = loader.max
  }

  private fun initList() {
    productListView.setHasStableIds(true)

    productListView.setViewTypes(listOf(
        ViewType(ViewType.ITEM_TYPE, layout.item_summary_product),
        ViewType(ViewType.FOOTER_TYPE, layout.item_attachment_file)
    ))
  }

  override fun inject(fragment: BaseFragment) {
    presenter =
        injector(context).with(
            Pair<AccrualContract.SummaryProductListView, Fragment>(this, fragment))
            .instance()
    imageRenderer = injector(context).with(fragment as Fragment)
        .instance()
    clmResourcesResolver = injector(context).instance()

  }

  companion object {
    val ENTRY = Architecture.Navigator.NavigationPoint(layout.screen_summary_product_list)
  }
}