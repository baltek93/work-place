package com.comarch.clm.mobileapp.accrual.presentation.scan


import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.graphics.ColorUtils
import androidx.fragment.app.Fragment
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.ScanProductViewState
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.accrual.R.layout
import com.comarch.clm.mobileapp.accrual.R.string
import com.comarch.clm.mobileapp.accrual.presentation.AccrualUiUtils
import com.comarch.clm.mobileapp.accrual.presentation.PointsTypesView.PointsTypeObject
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.BaseFragment
import com.comarch.clm.mobileapp.core.util.components.CLMConstraintLayout
import com.comarch.clm.mobileapp.core.util.components.CLMRelativeLayout
import com.comarch.clm.mobileapp.core.util.components.resources.CLMResourcesResolver
import com.comarch.clm.mobileapp.core.util.injector
import com.comarch.clm.mobileapp.core.util.view.HorizontalDividerItemDecoration
import com.comarch.clm.mobileapp.media.scanner.ScannerDetector
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.jakewharton.rxbinding2.view.RxView
import com.otaliastudios.cameraview.CameraView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.item_list_grid.view.cardView
import kotlinx.android.synthetic.main.item_scanner_product.view.errorLayout
import kotlinx.android.synthetic.main.item_scanner_product.view.mainLayout
import kotlinx.android.synthetic.main.item_scanner_product.view.nameValue
import kotlinx.android.synthetic.main.item_scanner_product.view.pointTypesView
import kotlinx.android.synthetic.main.item_scanner_product.view.quantityValue
import kotlinx.android.synthetic.main.item_scanner_product.view.statusValue
import kotlinx.android.synthetic.main.product_item.view.codeListView
import kotlinx.android.synthetic.main.screen_scan_product.view.buttonAction
import kotlinx.android.synthetic.main.screen_scan_product.view.cameraView
import kotlinx.android.synthetic.main.screen_scan_product.view.productsListEmpty
import kotlinx.android.synthetic.main.screen_scan_product.view.scannedCounter
import kotlinx.android.synthetic.main.screen_scan_product.view.scannerProductList
import kotlinx.android.synthetic.main.screen_scan_product.view.scannerProductListLayout
import kotlinx.android.synthetic.main.screen_scan_product.view.toolbar
import kotlinx.android.synthetic.main.screen_scan_product.view.totalItems

class ScanProductScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CLMRelativeLayout(
    context,
    attrs,
    defStyleAttr
), AccrualContract.ScanProductView {

  override lateinit var presenter: AccrualContract.ScanProductPresenter
  var clmResourcesResolver: CLMResourcesResolver = injector(context).instance()

  override fun render(state: ScanProductViewState) {

    if (state.productList.isEmpty()) {
      scannerProductListLayout.visibility = View.GONE
      productsListEmpty.visibility = View.VISIBLE
    } else {
      scannerProductListLayout.visibility = View.VISIBLE
      productsListEmpty.visibility = View.GONE
    }
    var totalItemsCount = 0
    state.productList.map { it.items.size }.iterator().forEach {
      totalItemsCount += it
    }

    totalItems.text = totalItemsCount.toString()
    scannedCounter.text = state.productList.size.toString()
    scannerProductList.render(object : Architecture.CLMListView.Renderable {
      override val size: Int = state.productList.size

      override fun bindView(view: View, position: Int) {
        val product = state.productList[position]

        view.quantityValue.text = product.items.size.toString()
        view.nameValue.text = product.name

        if (product.code != "" && !product.restricted) {
          view.mainLayout.layoutParams.height = (context.resources.getDimension(
              R.dimen.not_found_scanner_height_item)).toInt()

          view.errorLayout.visibility = View.GONE
          view.pointTypesView.visibility = View.VISIBLE
          if (product.points.isNotEmpty()) {
            view.pointTypesView.render(
                PointsTypeObject(pointsList = product.points, isGrid = true,
                    isWithBackground = false, isWithPointsType = false))
          }
        } else {
          view.mainLayout.layoutParams.height = (context.resources.getDimension(
              R.dimen.not_found_scanner_height_item)).toInt()
          view.errorLayout.setCardBackgroundColor(getTransparentColorForPointTypeBackground(
              clmResourcesResolver.resolveColor(string.styles_color_error)))
          view.nameValue.text = context.getString(string.labels_cma_accrual_productUnknown)
          view.errorLayout.visibility = View.VISIBLE
          view.pointTypesView.visibility = View.GONE
        }


        AccrualUiUtils.setStatusValue(view.statusValue, product,
            context.getString(string.labels_cma_accrual_productFound),
            context.getString(string.labels_cma_accrual_productNotFound), context.getString(
            string.labels_cma_accrual_productRestricted))

      }
    })
  }

  override fun getCameraViewFromView(): CameraView = cameraView

  override fun inject(fragment: BaseFragment) {
    presenter =
        injector(context).with(Pair<AccrualContract.ScanProductView, Fragment>(this, fragment))
            .instance()
//    presenter.openScanner()
  }

  override fun openScanner(detector: ScannerDetector) {
    detector.setupCamera()
  }

  override fun onDonePressed(): Observable<Any> {
    return RxView.clicks(buttonAction)
  }

  override fun init() {
    initList()
    toolbar.setColorBackIcon(string.styles_color_onPrimary)
  }

  private fun initList() {
    scannerProductList.setHasStableIds(true)
    scannerProductList.setLayout(layout.item_scanner_product)
    scannerProductList.addItemDecoration(
        HorizontalDividerItemDecoration(context, hasDividerBeforeFirstItem = true))
  }

  companion object {
    val ENTRY = Architecture.Navigator.NavigationPoint(layout.screen_scan_product)
  }

  private fun getTransparentColorForPointTypeBackground(color: Int): Int {
    return ColorUtils.setAlphaComponent(color, 50)
  }
}