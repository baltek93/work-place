package com.comarch.clm.mobileapp.accrual.data.model.realm

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class AccrualRealmModule
