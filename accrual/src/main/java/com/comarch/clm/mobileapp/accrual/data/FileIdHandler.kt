package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponsePoint
import com.comarch.clm.mobileapp.accrual.data.model.FileId
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualResponsePointImpl
import com.comarch.clm.mobileapp.accrual.data.model.realm.FileIdImpl
import com.comarch.clm.mobileapp.core.data.model.ModelHandler
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class FileIdHandler  : ModelHandler<FileId> {

  override fun instantiate(): FileId = FileIdImpl()

  override fun getClassMappings(): Map<Class<*>, Class<*>> {
    return mapOf(
    )
  }

  @FromJson
  fun toInterface(modelImpl: FileIdImpl): FileId {
    return modelImpl
  }

  @ToJson
  fun toImplementation(modelIf: FileId): FileIdImpl {
    return modelIf as FileIdImpl
  }
}