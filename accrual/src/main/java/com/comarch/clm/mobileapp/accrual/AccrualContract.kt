package com.comarch.clm.mobileapp.accrual

import android.content.Intent
import android.net.Uri
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProductPoint
import com.comarch.clm.mobileapp.accrual.data.model.AccrualRequest
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponse
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.InvoiceFile
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.Screen
import com.comarch.clm.mobileapp.core.data.model.Dictionary
import com.comarch.clm.mobileapp.core.data.model.Parameter
import com.comarch.clm.mobileapp.core.data.repository.filter.Predicate
import com.comarch.clm.mobileapp.core.presentation.BaseView
import com.comarch.clm.mobileapp.core.presentation.ErrorResult
import com.comarch.clm.mobileapp.core.presentation.SuccessResult
import com.comarch.clm.mobileapp.core.util.ClmOptional
import com.comarch.clm.mobileapp.core.util.components.CLMProgressButton
import com.comarch.clm.mobileapp.core.util.components.CLMProgressButton.State.ACTIVE
import com.comarch.clm.mobileapp.core.util.components.CLMProgressButton.State.PROGRESS
import com.comarch.clm.mobileapp.media.scanner.ScannerDetector
import com.otaliastudios.cameraview.CameraView
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.MultipartBody.Part
import java.io.Serializable

interface AccrualContract {


  interface AddInvoiceView : Screen<AddInvoicePresenter>, BaseView {
    fun render(state: AddInvoiceViewState)
    fun invoiceNoChanged(): Observable<String>
    fun onPressedAddFile(): Observable<Any>
    fun onPressedChooseGallery(): Observable<Any>
    fun onPressedChooseFile(): Observable<Any>
    fun onPressedMakePhoto(): Observable<Any>
    fun onPressedNext(): Observable<String>
    fun onPressedRemoveInvoiceFile(): Observable<Any>
    fun showChooseFile()

  }

  interface AddInvoicePresenter : Architecture.Presenter {
    fun saveInvoiceFile(uri: Uri, type: String = "application/image")
    fun openCrop(uri: Intent?)
    fun goToBigToFileDialog()
    fun uploadFile()

  }

  interface AddInvoiceViewModel : Architecture.ViewModel<AddInvoiceViewState> {
    fun saveInvoiceUri(uri: Uri, type: String)
    fun deleteInvoiceFile()
    fun saveInvoiceNumber(it: String?)
    fun changeInvoiceNumber(it: String?)
    fun uploadFile()

  }


  interface ScanProductView : Screen<ScanProductPresenter>, BaseView {
    fun render(state: ScanProductViewState)
    fun getCameraViewFromView(): CameraView
    fun openScanner(detector: ScannerDetector)
    fun onDonePressed(): Observable<Any>
  }

  interface ScanProductPresenter : Architecture.Presenter {
    fun openScanner()
  }

  interface ScanProductViewModel : Architecture.ViewModel<ScanProductViewState> {
    fun addProduct(code: String)
    fun getProducts()

  }

  interface AddProductView : Screen<AddProductPresenter>, BaseView {
    fun render(state: AddProductViewState)
    fun onPressedDone(): Observable<String>
  }

  interface AddProductPresenter : Architecture.Presenter

  interface AddProductViewModel : Architecture.ViewModel<AddProductViewState> {
    fun addProduct(it: String)

  }

  interface ProductListView : Screen<ProductListPresenter>, BaseView {
    fun render(state: ProductListViewState)
    fun onPressedScan(): Observable<Any>
    fun onPressedAddCode(): Observable<Any>
  }

  interface ProductListPresenter : Architecture.Presenter {
    fun onPressedScan()
    fun goToNextStep()
    fun onDeleteClicked(id: String)
  }

  interface ProductListViewModel : Architecture.ViewModel<ProductListViewState> {
    fun onDeleteClicked(id: String)

  }

  interface SummaryProductListView : Screen<SummaryProductListPresenter>, BaseView {
    fun render(state: SummaryProductListViewState)
    fun onPressedRegister(): Observable<Any>
  }

  interface SummaryProductListPresenter : Architecture.Presenter {
    fun onEditPressed()
  }

  interface SummaryProductListViewModel : Architecture.ViewModel<SummaryProductListViewState> {
    fun registerTransaction()
    fun deleteFileFromDataBase()
    fun errorMakeSale()

  }


  interface AccrualUseCase : Architecture.UseCase {
    fun getAccrualWizardProductList(): List<AccrualWizardProduct>
    fun getAccrualWizardProducts(
        isFoundProduct: Boolean = false): Observable<List<AccrualWizardProduct>>

    fun fetchWizardProduct(externalCode: String): Single<AccrualProduct>
    fun addWizardProduct(externalCode: String): Completable
    fun updateWizardProduct(externalCode: String, isUpdateName: Boolean = false): Completable
    fun deleteWizardProduct(id: String): Completable
    fun buildRequest(product: List<AccrualWizardProduct>,
        fileAttachment: InvoiceFile?): AccrualRequest

    fun makeAccrual(request: AccrualRequest, simulate: Boolean,
        isAddInvoice: Part? = null): Single<AccrualResponse>

    fun getParameter(code: String): Observable<ClmOptional<Parameter?>>
    fun uploadInvoice(avatar: Part): Single<String>
    fun getLocalObservableFileInvoice(): Observable<ClmOptional<InvoiceFile?>>
    fun getLocalFileInvoice(): InvoiceFile?

    fun saveInvoiceFile(path: String?, number: String?, type: String? = null): Completable
    fun deleteInvoiceFile(): Completable

  }

  interface AccrualRepository : Architecture.LocalRepository {
    fun getAccrualWizardProducts(
        predicate: Predicate? = null): Observable<List<AccrualWizardProduct>>

    fun getAccrualWizardProductList(): List<AccrualWizardProduct>

    fun fetchProduct(externalCode: String): Single<AccrualProduct>
    fun makeSale(accrualRequest: AccrualRequest, simulate: Boolean): Single<AccrualResponse>
    fun deleteWizardProduct(code: String): Completable
    fun deleteWizardProductsAndInvoiceFile()
    fun fetchParameter(code: String): Single<Parameter>
    fun uploadInvoice(avatar: Part): Single<String>
    fun getLocalFileInvoice(): Observable<ClmOptional<InvoiceFile?>>
    fun saveInvoiceFile(path: String?, number: String?, type: String?): Completable
    fun deleteInvoiceFile(): Completable


  }

  data class ProductListViewState(val productList: List<AccrualWizardProduct> = emptyList(),
      val isAddInvoice: Boolean = false, val isFoundProduct: Boolean = false)

  data class SummaryProductListViewState(val productList: List<AccrualWizardProduct> = emptyList(),
      val totalPoints: List<AccrualProductPoint> = emptyList(),
      val extraPoints: List<AccrualProductPoint> = emptyList(),
      val issuedPoints: Boolean = false,
      val isAddInvoice: Boolean = false,
      val fileAttachment: InvoiceFile? = null,
      val stateButton: CLMProgressButton.State = ACTIVE)

  data class AddProductViewState(val list: List<Dictionary> = emptyList())
  data class ScanProductViewState(val productList: List<AccrualWizardProduct> = emptyList(),
      var scannerEnabled: Boolean = true)

  data class AddInvoiceViewState(val list: List<Dictionary> = emptyList(),
      val invoiceUri: String? = null, val isAttachmentAttached: Boolean = false,
      val invoiceNumber: String? = null, val maxSizeFile: Int = 0,
      val isFileTooBig: Boolean = false)


  interface AccrualRoute : Serializable

  object ADD_PRODUCT_CODE_ROUTE : AccrualRoute
  object ADD_SCANNER_PRODUCT_CODE_ROUTE : AccrualRoute
  object SUMMARY_PRODUCT_LIST_ROUTE : AccrualRoute
  object ADD_INVOICE_ROUTE : AccrualRoute

  companion object {
    val ADD_PRODUCT_TAG = "ADD_PRODUCT_TAG"
    val MAKE_SALE_TAG = "MAKE_SALE_TAG"
    val SYNCHRONIZATION_TAG = "SYNCHRONIZATION_TAG"
    val ACCRUAL_ENABLED = "B2C_ACCRUAL_ENABLED"
    val ACCRUAL_TRN_CONFIRMATION_ENABLED = "B2C_ACCRUAL_TRN_CONFIRMATION_ENABLED"
    val ACCRUAL_TRN_CONFIRMATION_FILE_MAX_SIZE = "B2C_ACCRUAL_TRN_CONFIRMATION_FILE_MAX_SIZE"

    val ACCRUAL_POINT_TYPES = "POINT_TYPES"
    val CHOOSE_PHOTO_REQUEST_CODE = 876
    val CHOOSE_PDF_REQUEST_CODE = 456
  }

  class AddCodeSuccessResult : SuccessResult()
  class MakeSaleSuccessResult(val pointsList: List<AccrualProductPoint>?) : SuccessResult()
  class MakeSaleErrorResult(cause: Throwable) : ErrorResult(cause)


}