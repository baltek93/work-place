package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.accrual.data.model.AccrualRequest
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponse
import com.comarch.clm.mobileapp.accrual.data.model.FileId
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface AccrualTransactionsRemoteRepository {

  @POST("me/sales")
  fun makeSale(
      @Header("Authorization") authorization: String,
      @Body accrualRequest: AccrualRequest,
      @Query("simulate") simulate: Boolean): Single<AccrualResponse>

  @Multipart
  @POST("sales-import-invoice")
  fun importInvoice(      @Header("Authorization") authorization: String,@Part file: MultipartBody.Part): Single<FileId>
}
