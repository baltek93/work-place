package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponse
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualResponseImpl
import com.comarch.clm.mobileapp.core.data.model.ModelHandler
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class AccrualResponseHandler : ModelHandler<AccrualResponse> {

  override fun instantiate(): AccrualResponse = AccrualResponseImpl()

  override fun getClassMappings(): Map<Class<*>, Class<*>> {
    return mapOf(
        AccrualResponse::class.java to AccrualResponseImpl::class.java
    )
  }

  @FromJson
  fun toInterface(modelImpl: AccrualResponseImpl): AccrualResponse {
    return modelImpl
  }

  @ToJson
  fun toImplementation(modelIf: AccrualResponse): AccrualResponseImpl {
    return modelIf as AccrualResponseImpl
  }
}