package com.comarch.clm.mobileapp.accrual.data.model.realm

import com.comarch.clm.mobileapp.accrual.data.model.InvoiceFile
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProductPoint
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponse
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponsePoint
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProductItem
import com.comarch.clm.mobileapp.accrual.data.model.FileId
import com.comarch.clm.mobileapp.core.util.SINGLETON_ID
import com.squareup.moshi.Json
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.Date

open class AccrualProductImpl(
    @PrimaryKey override var code: String = "",
    override var name: String = "",
    override var description: String = "",
    override var partner: String = "",
    override var externalCode: String = "",
    override var resticted: Boolean = false,
    override var categoryCode: String = "",
    @field:Json(name = "points")
    var _points: RealmList<AccrualProductPointImpl> = RealmList()) : RealmObject(), AccrualProduct {
  override var points: List<AccrualProductPoint>
    get() = _points
    set(value) {
      // Checks if all items are of specified implementation type
      val typeFilteredValue = value.filterIsInstance<AccrualProductPointImpl>()
      require(typeFilteredValue.size == value.size)

      val realmList = RealmList<AccrualProductPointImpl>()
      realmList.addAll(typeFilteredValue)
      _points = realmList
    }
}

open class AccrualProductPointImpl(
    override var pointType: String = "",
    override var pointTypeName: String = "",
    override var points: Long = 0) : RealmObject(), AccrualProductPoint

open class AccrualWizardProductImpl(
    @PrimaryKey override var code: String = "",
    override var name: String = "",
    override var restricted: Boolean = false,
    override var addedDate: Date? = null,
    @field:Json(name = "items")
    var _items: RealmList<AccrualWizardProductItemImpl> = RealmList(),
    @field:Json(name = "points")
    var _points: RealmList<AccrualProductPointImpl> = RealmList()
) : RealmObject(), AccrualWizardProduct {
  override var issuedPoint: Boolean
    get() = points.firstOrNull { it.points > 0 } != null
    set(value) {

    }
  override var points: List<AccrualProductPoint>
    get() = _points
    set(value) {
      // Checks if all items are of specified implementation type
      val typeFilteredValue = value.filterIsInstance<AccrualProductPointImpl>()
      require(typeFilteredValue.size == value.size)

      val realmList = RealmList<AccrualProductPointImpl>()
      realmList.addAll(typeFilteredValue)
      _points = realmList
    }

  override var items: List<AccrualWizardProductItem>
    get() = _items
    set(value) {
      // Checks if all items are of specified implementation type
      val typeFilteredValue = value.filterIsInstance<AccrualWizardProductItemImpl>()
      require(typeFilteredValue.size == value.size)

      val realmList = RealmList<AccrualWizardProductItemImpl>()
      realmList.addAll(typeFilteredValue)
      _items = realmList
    }
}

open class AccrualWizardProductItemImpl(
    override var code: String = "",
    override var addedDate: Date? = null) : RealmObject(), AccrualWizardProductItem

open class AccrualResponseImpl(
    override var status: String = "",
    override var statusName: String = "",
    @field:Json(name = "transactionPoints")
    var _transactionPoints: AccrualResponsePointImpl? = AccrualResponsePointImpl(),
    override var comment: String = "") : RealmObject(), AccrualResponse {
  override var transactionPoints: AccrualResponsePoint?
    get() = _transactionPoints
    set(value) {
      require(value is AccrualResponsePointImpl)
      _transactionPoints = value
    }
}

open class AccrualResponsePointImpl(
    override var basicPoints: Int = 0,
    @field:Json(name = "points")
    var _points: RealmList<AccrualProductPointImpl> = RealmList()) : RealmObject(), AccrualResponsePoint {
  override var points: List<AccrualProductPoint>
    get() = _points
    set(value) {
      // Checks if all items are of specified implementation type
      val typeFilteredValue = value.filterIsInstance<AccrualProductPointImpl>()
      require(typeFilteredValue.size == value.size)

      val realmList = RealmList<AccrualProductPointImpl>()
      realmList.addAll(typeFilteredValue)
      _points = realmList
    }
}

open class InvoiceFileImpl(
    override var path: String? = null,
    @PrimaryKey
    override var id: Long = SINGLETON_ID,
    override var invoiceNo: String? = null,
    override var type: String? = null) : RealmObject(), InvoiceFile

open class FileIdImpl(
    override var fileId: String = ""
) : FileId
