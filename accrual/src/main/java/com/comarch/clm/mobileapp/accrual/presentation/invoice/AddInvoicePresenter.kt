package com.comarch.clm.mobileapp.accrual.presentation.invoice

import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.webkit.MimeTypeMap
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddInvoiceViewState
import com.comarch.clm.mobileapp.accrual.AccrualContract.Companion.CHOOSE_PDF_REQUEST_CODE
import com.comarch.clm.mobileapp.accrual.AccrualContract.Companion.CHOOSE_PHOTO_REQUEST_CODE
import com.comarch.clm.mobileapp.accrual.AccrualContract.SUMMARY_PRODUCT_LIST_ROUTE
import com.comarch.clm.mobileapp.accrual.presentation.invoice.FileTooBigDialog.OnBigTooFile
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.BasePresenter
import com.comarch.clm.mobileapp.media.MediaContract.MediaPresenter
import com.comarch.clm.mobileapp.media.MediaController.CropperListener
import com.theartofdev.edmodo.cropper.CropImageView.CropShape
import com.theartofdev.edmodo.cropper.CropImageView.CropShape.RECTANGLE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import java.io.File
import java.util.concurrent.TimeUnit.MILLISECONDS


class AddInvoicePresenter(
    val view: AccrualContract.AddInvoiceView,
    val router: Architecture.Router,
    viewModel: AccrualContract.AddInvoiceViewModel,
    uiEventHandler: Architecture.UiEventHandler,
    val mediaPresenter: MediaPresenter,
    val navigator: Architecture.Navigator<Architecture.Navigator.NavigationScreen>
) : BasePresenter<AddInvoiceViewState, AccrualContract.AddInvoiceViewModel>(
    viewModel,
    uiEventHandler
), AccrualContract.AddInvoicePresenter, OnBigTooFile {

  init {
    view.onPressedAddFile().subscribe {
    }.addTo(disposables)

    view.invoiceNoChanged()
        .debounce(300, MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {
          viewModel.changeInvoiceNumber(it)
        }.addTo(disposables)

    view.onPressedNext().subscribe {
      viewModel.saveInvoiceNumber(it)

      router.nextScreen(SUMMARY_PRODUCT_LIST_ROUTE)

    }.addTo(disposables)
    view.onPressedMakePhoto().subscribe {
      mediaPresenter.startCameraActivity(InvoiceCropperListener(this), RECTANGLE)

    }.addTo(disposables)
    view.onPressedChooseFile().subscribe {
      mediaPresenter.chooseFile(arrayOf("application/pdf"), CHOOSE_PDF_REQUEST_CODE)
    }.addTo(disposables)
    view.onPressedChooseGallery().subscribe {
      mediaPresenter.chooseGallery(arrayOf("image/*"), CHOOSE_PHOTO_REQUEST_CODE)
    }.addTo(disposables)

    view.onPressedRemoveInvoiceFile().subscribe {
      viewModel.deleteInvoiceFile()
    }.addTo(disposables)
  }


  override fun onViewStateChanged(state: AddInvoiceViewState) {
    view.render(state)
  }

  override fun saveInvoiceFile(uri: Uri, type: String) {
    val type = type(uri)
    if (isTypeSupported(type)) {
      viewModel.saveInvoiceUri(uri, type!!)
    } else {
      navigator.showCLMDialog(FileNotSupportedDialog.newInstance())
    }
  }

  private fun isTypeSupported(type: String?): Boolean {
    return type?.contains("pdf") == true ||
        type?.contains("jpg") == true ||
        type?.contains("png") == true ||
        type?.contains("jpeg") == true ||
        type?.contains("image") == true
  }

  private fun type(uri: Uri): String? {
    val cR = view.getContext().contentResolver
    var type = view.getContext().contentResolver.getType(uri)
    if (type == null) {
      val extension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
      if (extension != null) {
        type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
      }
    }
    val extension = cR.getType(uri)
    return type
  }

  override fun openCrop(data: Intent?) {
    data?.let {
      val type = type(it.data!!)
      if (isTypeSupported(type)) {
        mediaPresenter.startImageCropper(it, InvoiceCropperListener(this), RECTANGLE)
      } else {
        navigator.showCLMDialog(FileNotSupportedDialog.newInstance())
      }

    }
  }

  override fun goToBigToFileDialog() {
    navigator.showCLMDialog(FileTooBigDialog.newInstance(this))
  }

  override fun uploadFile() {
    viewModel.uploadFile()
  }

  override fun onAddFile() {
    view.showChooseFile()
  }


  fun getPathFromUri(context: Context, uri: Uri): String? {

    // DocumentProvider
    if (DocumentsContract.isDocumentUri(context, uri)) {
      // ExternalStorageProvider
      if (isExternalStorageDocument(uri)) {
        val docId: String = DocumentsContract.getDocumentId(uri)
        val split = docId.split(":").toTypedArray()
        val type = split[0]
        if ("primary".equals(type, ignoreCase = true)) {
          return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
        }

        // TODO handle non-primary volumes
      } else if (isDownloadsDocument(uri)) {
        val id: String = DocumentsContract.getDocumentId(uri)
        val contentUri: Uri = ContentUris.withAppendedId(
            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
        return getDataColumn(context, contentUri, null, null)
      } else if (isMediaDocument(uri)) {
        val docId: String = DocumentsContract.getDocumentId(uri)
        val split = docId.split(":").toTypedArray()
        val type = split[0]
        var contentUri: Uri? = null
        if ("image" == type) {
          contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        } else if ("video" == type) {
          contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        } else if ("audio" == type) {
          contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        }
        val selection = "_id=?"
        val selectionArgs = arrayOf(
            split[1]
        )
        return getDataColumn(context, contentUri, selection, selectionArgs)
      }
    } else if ("content".equals(uri.scheme, ignoreCase = true)) {

      // Return the remote address
      return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null,
          null)
    } else if ("file".equals(uri.scheme, ignoreCase = true)) {
      return uri.path
    }
    return null
  }

  fun getDataColumn(context: Context, uri: Uri?, selection: String?,
      selectionArgs: Array<String>?): String? {
    var cursor: Cursor? = null
    val column = "_data"
    val projection = arrayOf(
        column
    )
    try {
      cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs,
          null)
      if (cursor != null && cursor.moveToFirst()) {
        val index: Int = cursor.getColumnIndexOrThrow(column)
        return cursor.getString(index)
      }
    } finally {
      if (cursor != null) cursor.close()
    }
    return null
  }


  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is ExternalStorageProvider.
   */
  fun isExternalStorageDocument(uri: Uri): Boolean {
    return "com.android.externalstorage.documents" == uri.authority
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is DownloadsProvider.
   */
  fun isDownloadsDocument(uri: Uri): Boolean {
    return "com.android.providers.downloads.documents" == uri.authority
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is MediaProvider.
   */
  fun isMediaDocument(uri: Uri): Boolean {
    return "com.android.providers.media.documents" == uri.authority
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is Google Photos.
   */
  fun isGooglePhotosUri(uri: Uri): Boolean {
    return "com.google.android.apps.photos.content" == uri.authority
  }


}


class InvoiceCropperListener(val presenter: AddInvoicePresenter) : CropperListener {
  override fun onUri(uri: Uri) {
    presenter.saveInvoiceFile(uri)
  }

  override fun onError(errorMessage: String) {
  }

}


//var cR = view.getContext().contentResolver
//var mime = MimeTypeMap.getSingleton()
////var type = mime.getExtensionFromMimeType(cR.getType(uri))
//val returnCursor = cR.query(uri,null, null, null, null)
//returnCursor.moveToFirst()
//returnCursor.getString(returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
//DocumentsContract.isDocumentUri(view.getContext(), uri)
//val docId = DocumentsContract.getDocumentId(uri)
//docId.split(":")
//Environment.getExternalStorageDirectory()