package com.comarch.clm.mobileapp.accrual.presentation.invoice

import android.view.View
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.core.presentation.dialog.CLMDialog
import kotlinx.android.synthetic.main.dialog_file_not_supported.view.okButton

class FileNotSupportedDialog : CLMDialog() {

  override val isRoundDialog: Boolean = true
  private lateinit var dialogView: View

  override fun getLayout(): View {
    dialogView = activity?.layoutInflater?.inflate(R.layout.dialog_file_not_supported, null)!!
    dialogView.okButton.setOnClickListener {
      dismiss()
    }
    return dialogView
  }

  companion object {
    fun newInstance(): FileNotSupportedDialog {
      val dialog = FileNotSupportedDialog()
      return dialog
    }
  }

}
