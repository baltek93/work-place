package com.comarch.clm.mobileapp.accrual.presentation

import android.app.Application
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.ProductListViewModel
import com.comarch.clm.mobileapp.accrual.AccrualContract.ProductListViewState
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProductPoint
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualProductPointImpl
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualWizardProductImpl
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel
import com.comarch.clm.mobileapp.core.presentation.ViewModelCompletableObserver
import com.comarch.clm.mobileapp.core.presentation.ViewModelObserver
import com.comarch.clm.mobileapp.core.presentation.dashboard.DashboardComponentObserver
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import io.reactivex.rxkotlin.addTo
import io.realm.RealmList

class ProductListViewModel(
    var app: Application) : BaseViewModel<ProductListViewState>(
    app), ProductListViewModel {

  private val useCase = injector(app).instance<AccrualContract.AccrualUseCase>()

  override fun getDefaultViewState(): ProductListViewState = ProductListViewState()

  init {

    useCase.getAccrualWizardProductList().forEach {
      it.items.forEach {
        useCase.updateWizardProduct(it.code, true).subscribeWith(
            ViewModelCompletableObserver(this) {
            }).addTo(disposables)
      }
    }
    useCase.getParameter(AccrualContract.ACCRUAL_TRN_CONFIRMATION_ENABLED).map {
      it.value?.value == "true"
    }
        .subscribeWith(ViewModelObserver(this) {
          setState(getState().copy(isAddInvoice = it))
          if (it == null || it == false) {
            useCase.deleteInvoiceFile().subscribeWith(ViewModelCompletableObserver(this)).addTo(
                disposables)
          }
        })
        .addTo(disposables)
    useCase.getAccrualWizardProducts().subscribeWith(ViewModelObserver(this)
    {
      setState(getState().copy(productList = it))
    }).addTo(disposables)
    useCase.getAccrualWizardProducts(true).subscribeWith(
        ViewModelObserver(this)
        {
          setState(getState().copy(isFoundProduct = it.isNotEmpty()))
        }).addTo(disposables)
  }

  override fun onDeleteClicked(id: String) {
    useCase.deleteWizardProduct(id).subscribeWith(ViewModelCompletableObserver(this)
    {
    }).addTo(disposables)
  }


}