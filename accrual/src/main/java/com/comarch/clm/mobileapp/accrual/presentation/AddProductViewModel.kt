package com.comarch.clm.mobileapp.accrual.presentation

import android.app.Application
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddCodeSuccessResult
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddProductViewState
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel
import com.comarch.clm.mobileapp.core.presentation.ViewModelCompletableObserver
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import io.reactivex.rxkotlin.addTo

class AddProductViewModel(
    var app: Application) : BaseViewModel<AddProductViewState>(
    app), AccrualContract.AddProductViewModel {

  private val useCase = injector(app).instance<AccrualContract.AccrualUseCase>()


  override fun getDefaultViewState(): AddProductViewState = AddProductViewState()
  override fun addProduct(it: String) {
    useCase.addWizardProduct(it).subscribeWith(ViewModelCompletableObserver(this)
    {
      postEvent(AddCodeSuccessResult())
    }).addTo(disposables)
  }




}