package com.comarch.clm.mobileapp.accrual.presentation.dashboard

import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AccrualUseCase
import com.comarch.clm.mobileapp.accrual.AccrualContract.Companion
import com.comarch.clm.mobileapp.core.presentation.dashboard.BaseDashboardComponentModel
import com.comarch.clm.mobileapp.core.presentation.dashboard.DashboardComponentObserver
import io.reactivex.rxkotlin.addTo

class AccrualWizardDashboardComponentModel (
    val useCase: AccrualUseCase) : BaseDashboardComponentModel<Boolean>() {

  override fun runTasks() {
      useCase.getParameter(AccrualContract.ACCRUAL_ENABLED).map {
        it.value?.value == "true"
      }
          .subscribeWith(DashboardComponentObserver(this))
          .addTo(disposables)

  }
}