package com.comarch.clm.mobileapp.accrual.presentation

import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddCodeSuccessResult
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddProductViewState
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.BasePresenter
import com.comarch.clm.mobileapp.core.presentation.SuccessResult
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class AddProductPresenter(
    val view: AccrualContract.AddProductView,
    val router: Architecture.Router,
    viewModel: AccrualContract.AddProductViewModel,
    uiEventHandler: Architecture.UiEventHandler
) : BasePresenter<AddProductViewState, AccrualContract.AddProductViewModel>(
    viewModel,
    uiEventHandler
), AccrualContract.AddProductPresenter {

  init {
    view.onPressedDone().subscribeBy {
      viewModel.addProduct(it)
    }.addTo(disposables)
  }

  override fun onViewStateChanged(state: AddProductViewState) {
    view.render(state)
  }

  override fun handleSuccessEvent(successResult: SuccessResult) {
    if (successResult is AddCodeSuccessResult) {
      router.previousScreen()
    }
    super.handleSuccessEvent(successResult)
  }
}