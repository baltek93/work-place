package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.core.data.model.Parameter
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface AccrualSynchronizationRemoteRepository
{
  @GET("parameters/{code}")
  fun fetchParameter(
      @Header("Authorization") authorization: String,
      @Path("code") code: String) : Single<Parameter>
}