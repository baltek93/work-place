package com.comarch.clm.mobileapp.accrual

import androidx.fragment.app.Fragment
import com.comarch.clm.mobileapp.accrual.AccrualContract.Companion.ADD_PRODUCT_TAG
import com.comarch.clm.mobileapp.accrual.AccrualContract.Companion.MAKE_SALE_TAG
import com.comarch.clm.mobileapp.accrual.AccrualContract.Companion.SYNCHRONIZATION_TAG
import com.comarch.clm.mobileapp.accrual.data.AccrualProductHandler
import com.comarch.clm.mobileapp.accrual.data.AccrualProductsRemoteRepository
import com.comarch.clm.mobileapp.accrual.data.AccrualRepository
import com.comarch.clm.mobileapp.accrual.data.AccrualResponseHandler
import com.comarch.clm.mobileapp.accrual.data.AccrualResponsePointHandler
import com.comarch.clm.mobileapp.accrual.data.AccrualSynchronizationRemoteRepository
import com.comarch.clm.mobileapp.accrual.data.AccrualTransactionsRemoteRepository
import com.comarch.clm.mobileapp.accrual.data.AccrualWizardProductHandler
import com.comarch.clm.mobileapp.accrual.data.FileIdHandler
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponse
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponsePoint
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.FileId
import com.comarch.clm.mobileapp.accrual.domain.AccrualUseCase
import com.comarch.clm.mobileapp.accrual.presentation.AddProductPresenter
import com.comarch.clm.mobileapp.accrual.presentation.AddProductViewModel
import com.comarch.clm.mobileapp.accrual.presentation.ProductListPresenter
import com.comarch.clm.mobileapp.accrual.presentation.ProductListViewModel
import com.comarch.clm.mobileapp.accrual.presentation.SummaryProductListPresenter
import com.comarch.clm.mobileapp.accrual.presentation.SummaryProductListViewModel
import com.comarch.clm.mobileapp.accrual.presentation.invoice.AddInvoicePresenter
import com.comarch.clm.mobileapp.accrual.presentation.invoice.AddInvoiceViewModel
import com.comarch.clm.mobileapp.accrual.presentation.scan.ScanProductPresenter
import com.comarch.clm.mobileapp.accrual.presentation.scan.ScanProductViewModel
import com.comarch.clm.mobileapp.core.Architecture.CustomView
import com.comarch.clm.mobileapp.core.data.model.ModelAdapter
import com.comarch.clm.mobileapp.core.data.model.ModelHandler
import com.comarch.clm.mobileapp.core.data.model.RestConfiguration
import com.comarch.clm.mobileapp.core.data.model.returnBaseUrl
import com.comarch.clm.mobileapp.core.util.viewModelOf
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.factory
import com.github.salomonbrys.kodein.inSet
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import com.github.salomonbrys.kodein.singleton
import com.github.salomonbrys.kodein.with
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

val accrualDependency = Kodein.Module {

  bind<ModelHandler<AccrualResponse>>() with singleton {
    AccrualResponseHandler()
  }

  bind<ModelAdapter>().inSet() with provider {
    instance<ModelHandler<AccrualResponse>>()
  }
  bind<ModelHandler<AccrualResponsePoint>>() with singleton {
    AccrualResponsePointHandler()
  }

  bind<ModelAdapter>().inSet() with provider {
    instance<ModelHandler<AccrualResponsePoint>>()
  }

  bind<ModelHandler<FileId>>() with singleton {
    FileIdHandler()
  }

  bind<ModelAdapter>().inSet() with provider {
    instance<ModelHandler<FileId>>()
  }

  bind<ModelHandler<AccrualProduct>>() with singleton {
    AccrualProductHandler()
  }

  bind<ModelAdapter>().inSet() with provider {
    instance<ModelHandler<AccrualProduct>>()
  }

  bind<ModelHandler<AccrualWizardProduct>>() with singleton {
    AccrualWizardProductHandler()
  }

  bind<ModelAdapter>().inSet() with provider {
    instance<ModelHandler<AccrualWizardProduct>>()
  }


  bind<AccrualContract.ScanProductPresenter>() with factory { dependency: Pair<AccrualContract.ScanProductView, Fragment> ->
    ScanProductPresenter(
        dependency.first, instance(), with(dependency.second).instance(),
        with(dependency.first as CustomView).instance(), instance(), instance(),
    )
  }

  bind<AccrualContract.ScanProductViewModel>() with factory { fragment: Fragment ->
    viewModelOf(fragment, ScanProductViewModel::class.java)
  }

  bind<AccrualContract.AddProductPresenter>() with factory { dependency: Pair<AccrualContract.AddProductView, Fragment> ->
    AddProductPresenter(dependency.first, instance(), with(dependency.second).instance(),
        with(dependency.first as CustomView).instance())
  }

  bind<AccrualContract.AddProductViewModel>() with factory { fragment: Fragment ->
    viewModelOf(fragment, AddProductViewModel::class.java)
  }

  bind<AccrualContract.ProductListPresenter>() with factory { dependency: Pair<AccrualContract.ProductListView, Fragment> ->
    ProductListPresenter(dependency.first, instance(), with(dependency.second).instance(),
        with(dependency.first as CustomView).instance())
  }

  bind<AccrualContract.ProductListViewModel>() with factory { fragment: Fragment ->
    viewModelOf(fragment, ProductListViewModel::class.java)
  }

  bind<AccrualContract.SummaryProductListPresenter>() with factory { dependency: Pair<AccrualContract.SummaryProductListView, Fragment> ->
    SummaryProductListPresenter(dependency.first, instance(), with(dependency.second).instance(),
        with(dependency.first as CustomView).instance())
  }

  bind<AccrualContract.SummaryProductListViewModel>() with factory { fragment: Fragment ->
    viewModelOf(fragment, SummaryProductListViewModel::class.java)
  }

  bind<AccrualContract.AccrualRepository>() with provider {
    AccrualRepository(instance(), instance(), instance(), instance(), instance())
  }
  bind<AccrualContract.AccrualUseCase>() with provider {
    AccrualUseCase(instance(), instance(), instance(), instance(), instance(), instance())
  }


  bind<AccrualContract.AddInvoicePresenter>() with factory { dependency: Pair<AccrualContract.AddInvoiceView, Fragment> ->
    AddInvoicePresenter(
        dependency.first, instance(), with(dependency.second).instance(),
        with(dependency.first as CustomView).instance(), instance(),instance())

  }

  bind<AccrualContract.AddInvoiceViewModel>() with factory { fragment: Fragment ->
    viewModelOf(fragment, AddInvoiceViewModel::class.java)
  }

  bind<Retrofit>(tag = ADD_PRODUCT_TAG) with singleton {
    Retrofit.Builder()
        .baseUrl(returnBaseUrl(instance(tag = ADD_PRODUCT_TAG)))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(
            MoshiConverterFactory.create(instance())
        )
        .client(instance())
        .build()
  }
  bind<RestConfiguration>(tag = ADD_PRODUCT_TAG) with instance(
//      RestConfiguration(url = "https://extensions-tenant-6.dev01.clmcloud.krakow.comarch/product/", version = "v1.2")
      RestConfiguration(url = "https://extensions-demo-gamma.eu3.loyalty-comarch.com/product/",
          version = "v1.2")
  )

  bind<Retrofit>(tag = MAKE_SALE_TAG) with singleton {
    Retrofit.Builder()
        .baseUrl(returnBaseUrl(instance(tag = MAKE_SALE_TAG)))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(
            MoshiConverterFactory.create(instance())
        )
        .client(instance())
        .build()
  }

  bind<RestConfiguration>(tag = MAKE_SALE_TAG) with instance(
//      RestConfiguration(url = "https://extensions-tenant-6.dev01.clmcloud.krakow.comarch/trnprocessor/", version = "v1.2")
      RestConfiguration(url = "https://extensions-demo-gamma.eu3.loyalty-comarch.com/trnprocessor/",
          version = "v1.2")
  )
  bind<Retrofit>(tag = SYNCHRONIZATION_TAG) with singleton {
    Retrofit.Builder()
        .baseUrl(returnBaseUrl(instance(tag = SYNCHRONIZATION_TAG)))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(
            MoshiConverterFactory.create(instance())
        )
        .client(instance())
        .build()
  }

  bind<RestConfiguration>(tag = SYNCHRONIZATION_TAG) with instance(
//      RestConfiguration(url = "https://extensions-tenant-6.dev01.clmcloud.krakow.comarch/dictionary/", version = "v1.2")
      RestConfiguration(url = "https://extensions-demo-gamma.eu3.loyalty-comarch.com/dictionary/",
          version = "v1.2")
  )
  bind<AccrualProductsRemoteRepository>() with provider {
    instance<Retrofit>(tag = ADD_PRODUCT_TAG).create(
        AccrualProductsRemoteRepository::class.java)
  }
  bind<AccrualTransactionsRemoteRepository>() with provider {
    instance<Retrofit>(tag = MAKE_SALE_TAG).create(
        AccrualTransactionsRemoteRepository::class.java)
  }

  bind<AccrualSynchronizationRemoteRepository>() with provider {
    instance<Retrofit>(tag = SYNCHRONIZATION_TAG).create(
        AccrualSynchronizationRemoteRepository::class.java)
  }
}