package com.comarch.clm.mobileapp.accrual.presentation

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.fragment.app.Fragment
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.ProductListViewState
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.accrual.R.layout
import com.comarch.clm.mobileapp.accrual.R.string
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProductPoint
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProductItem
import com.comarch.clm.mobileapp.accrual.presentation.PointsTypesView.PointsTypeObject
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.BaseFragment
import com.comarch.clm.mobileapp.core.util.components.CLMConstraintLayout
import com.comarch.clm.mobileapp.core.util.components.CLMLabel
import com.comarch.clm.mobileapp.core.util.components.styles.CLMButtonStyles
import com.comarch.clm.mobileapp.core.util.injector
import com.comarch.clm.mobileapp.core.util.view.HorizontalDividerItemDecoration
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.google.android.material.appbar.AppBarLayout
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import kotlinx.android.synthetic.main.item_code_product.view.codeProduct
import kotlinx.android.synthetic.main.item_code_product.view.numberLabel
import kotlinx.android.synthetic.main.item_code_product.view.statusLabel
import kotlinx.android.synthetic.main.product_item.view.codeListView
import kotlinx.android.synthetic.main.product_item.view.iconError
import kotlinx.android.synthetic.main.product_item.view.labelError
import kotlinx.android.synthetic.main.product_item.view.mainLayout
import kotlinx.android.synthetic.main.product_item.view.nameLabel
import kotlinx.android.synthetic.main.product_item.view.pointTypesViewTest
import kotlinx.android.synthetic.main.product_item.view.quantityValue
import kotlinx.android.synthetic.main.screen_product_list.view.addProductButton
import kotlinx.android.synthetic.main.screen_product_list.view.appbar
import kotlinx.android.synthetic.main.screen_product_list.view.emptyProductLayout
import kotlinx.android.synthetic.main.screen_product_list.view.informationProductInvoice
import kotlinx.android.synthetic.main.screen_product_list.view.loader
import kotlinx.android.synthetic.main.screen_product_list.view.productListView
import kotlinx.android.synthetic.main.screen_product_list.view.productListTitle
import kotlinx.android.synthetic.main.screen_product_list.view.scanBarCodeButton
import kotlinx.android.synthetic.main.screen_product_list.view.toolbar
import kotlinx.android.synthetic.main.view_action_button.view.buttonAction
import kotlinx.android.synthetic.main.view_action_button.view.buttonActionSecond
import kotlinx.android.synthetic.main.view_error_information.view.labelInformation

class ProductListScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CLMConstraintLayout(
    context,
    attrs,
    defStyleAttr
), AccrualContract.ProductListView {

  override lateinit var presenter: AccrualContract.ProductListPresenter

  override fun render(state: ProductListViewState) {
    if (state.isAddInvoice) {
      loader.max = 3
      loader.progress = 2
      loader.progress = 1
      informationProductInvoice.visibility = View.VISIBLE
      informationProductInvoice.labelInformation.text = context.getString(
          string.labels_cma_accrual_one_invoice_information)

    } else {
      loader.max = 2
      loader.progress = 2
      loader.progress = 1
      informationProductInvoice.visibility = View.GONE

    }

    if (state.productList.isNotEmpty()) {
      toolbar.onRightCustomButtonClick = {
        presenter.onPressedScan()
      }
      toolbar.setCustomButton(context.getString(string.labels_cma_accrual_wizardProductsScanner_codeScan))
      toolbar.setStyleCustomButton(CLMButtonStyles.BUTTON_BORDERLESS_SECONDARY)
      productListView.render(object : Architecture.CLMListView.Renderable {
        override val size: Int = state.productList.size

        override fun bindView(view: View, position: Int) {
          val product = state.productList[position]

          view.quantityValue.text = product.items.size.toString()
          view.nameLabel.text = product.name
          view.codeListView.addItemDecoration(
              HorizontalDividerItemDecoration(context, hasDividerBeforeFirstItem = true))
          view.codeListView.setHasStableIds(true)
          view.codeListView.setLayout(layout.item_code_product)

          view.codeListView.layoutParams.height = (product.items.size * context.resources.getDimension(
              R.dimen.code_product_height_item)).toInt()

          if (product.code != "" && !product.restricted) {
            view.iconError.visibility = View.GONE
            view.labelError.visibility = View.GONE
            view.codeListView.render(CodeRenderer(product, product.items))
            view.layoutParams.height = (context.resources.getDimension(
                R.dimen.product_height_item) + product.items.size * context.resources.getDimension(
                R.dimen.code_product_height_item)).toInt()
          } else {
            view.nameLabel.text = context.resources.getString(
                string.labels_cma_accrual_productUnknown)
            view.iconError.visibility = View.VISIBLE
            view.labelError.visibility = View.VISIBLE
            view.codeListView.render(CodeRenderer(product, product.items.distinctBy { it.code }))
            view.layoutParams.height = (context.resources.getDimension(
                R.dimen.product_not_found_height_item) + product.items.size * context.resources.getDimension(
                R.dimen.code_product_height_item)).toInt()
          }
          view.mainLayout.layoutParams.height = view.layoutParams.height



          if (product.points.isNotEmpty()) {
            view.pointTypesViewTest.render(
                PointsTypeObject(pointsList = product.points, isGrid = true,
                    labelText = createStringSummary(product.points, product.items.size)))
          }
        }


        override fun onSwipeLayoutButtonClick(item: View, itemPos: Int, childPos: Int) {
          val id = state.productList[itemPos].code
          if (childPos == 0) {
            presenter.onDeleteClicked(id)
          }
        }
      })
      productListView.visibility = View.VISIBLE
      buttonActionSecond.visibility = View.VISIBLE
      emptyProductLayout.visibility = View.GONE
    } else {
      emptyProductLayout.visibility = View.VISIBLE
      productListView.visibility = View.GONE
      buttonActionSecond.visibility = View.GONE
    }

    buttonAction.setStateEnabled(state.isFoundProduct)
  }


  override fun onPressedScan(): Observable<Any> {
    return RxView.clicks(scanBarCodeButton)
  }

  override fun onPressedAddCode(): Observable<Any> {
    return Observable.merge(RxView.clicks(addProductButton), RxView.clicks(buttonActionSecond))
  }

  override fun init() {
    initList()
    loader.max = 2
    loader.progress = 1
    loader.progress = 1
  }

  override fun inject(fragment: BaseFragment) {
    presenter =
        injector(context).with(Pair<AccrualContract.ProductListView, Fragment>(this, fragment))
            .instance()
    buttonActionSecond.visibility = View.VISIBLE
    buttonAction.setOnClickListener {
      presenter.goToNextStep()
    }
  }

  private fun initList() {
    productListView.animateOnSwipeLayoutRebind = true
    productListView.setHasStableIds(true)
    productListView.addItemDecoration(
        HorizontalDividerItemDecoration(context, hasDividerBeforeFirstItem = true))
    productListView.setLayout(layout.main_item_list_product)
  }

  private fun initTitle() {
    val mListener = AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
      if (productListTitle.height + verticalOffset <= 1) {
        productListTitle.animate().alpha(0f)
        toolbar.findViewById<CLMLabel>(R.id.toolbar_title).animate().alpha(1f)
        toolbar.setTitle(string.labels_cma_accrual_productList)
      } else {
        toolbar.findViewById<CLMLabel>(R.id.toolbar_title).animate().alpha(0f)
        toolbar.setTitle("")
        productListTitle.animate().alpha(1f)
      }
    }
    appbar.addOnOffsetChangedListener(mListener)
  }

  companion object {
    val ENTRY = Architecture.Navigator.NavigationPoint(layout.screen_product_list)

    fun createStringSummary(pointsList: List<AccrualProductPoint>, quantity: Int): String {
      var string = "$quantity x ("
      pointsList.forEachIndexed { index, accrualProductPoint ->
        string += " ${(accrualProductPoint.points / quantity)} ${accrualProductPoint.pointTypeName}"
        if (index != pointsList.size - 1)
          string += " +"
      }
      string += ")"
      return string
    }
  }

}

class CodeRenderer(val model: AccrualWizardProduct,
    val items: List<AccrualWizardProductItem>) : Architecture.CLMListView.Renderable {
  override val size: Int = items.size
  override fun bindView(view: View, position: Int) {
    val productCode = items[position]
    val pagination = position + 1
    view.numberLabel.text = "$pagination."
    view.codeProduct.text = productCode.code
    AccrualUiUtils.setStatusValue(view.statusLabel, model,
        view.context.getString(string.labels_cma_accrual_productFound),
        view.context.getString(string.labels_cma_accrual_productNotFound),
        view.context.getString(
            string.labels_cma_accrual_productRestricted))
  }

}