package com.comarch.clm.mobileapp.accrual.data.model

import com.comarch.clm.mobileapp.core.data.model.BasePoint
import java.io.Serializable
import java.util.Date

interface AccrualProduct {
  val categoryCode:String
  val code: String
  val name: String
  val description: String
  val partner: String
  val externalCode: String
  val resticted: Boolean
  var points: List<AccrualProductPoint>
}

interface AccrualProductPoint : BasePoint

interface AccrualWizardProduct {
  val code: String
  var name: String
  val restricted: Boolean
  var addedDate: Date?
  var items: List<AccrualWizardProductItem>
  var points: List<AccrualProductPoint>
  //local
  var issuedPoint:Boolean
  companion object
  {
    const val FIELD_RESTRICTED = "restricted"
    const val FIELD_CODE = "code"
  }
}

interface AccrualWizardProductItem {
  val code: String
  val addedDate: Date?
}

interface AccrualResponse {
  val status: String
  val statusName: String
  val transactionPoints: AccrualResponsePoint?
  val comment: String
}

interface AccrualResponsePoint {
  val basicPoints: Int
  val points: List<AccrualProductPoint>
}

data class AccrualRequest(
    val products: List<AccrualRequestProduct>,
    val comment: String?,
    var fileId:String?,
    var trnNo:String?)

data class AccrualRequestProduct(
    val code: String?=null,
    val externalCode: String?=null,
    val quantity: Int
)
interface InvoiceFile{
  var path:String?
  val id:Long
  var invoiceNo:String?
  var type:String?

}

interface FileId{
  val fileId:String
}



