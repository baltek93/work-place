package com.comarch.clm.mobileapp.accrual.presentation

import android.content.Context
import android.util.AttributeSet
import androidx.core.graphics.ColorUtils
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.core.util.components.CLMConstraintLayout
import com.comarch.clm.mobileapp.core.util.components.resources.CLMResourcesResolver
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.view_points_type.view.rootCardView

class ErrorInformationView @kotlin.jvm.JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CLMConstraintLayout(context, attrs, defStyleAttr) {
  var clmResourcesResolver: CLMResourcesResolver = injector(context).instance()

  init {
    inflate(context, R.layout.view_error_information, this)
    rootCardView.setCardBackgroundColor(getTransparentColorForPointTypeBackground(
        clmResourcesResolver.resolveColor(R.string.styles_color_error)))
  }



  companion object{
    fun getTransparentColorForPointTypeBackground(color: Int): Int {
      return ColorUtils.setAlphaComponent(color, 50)
    }
  }
}