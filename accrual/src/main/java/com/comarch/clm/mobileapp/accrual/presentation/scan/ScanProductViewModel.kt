package com.comarch.clm.mobileapp.accrual.presentation.scan

import android.app.Application
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddCodeSuccessResult
import com.comarch.clm.mobileapp.accrual.AccrualContract.ScanProductViewState
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualProductImpl
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualProductPointImpl
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualWizardProductImpl
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel
import com.comarch.clm.mobileapp.core.presentation.ViewModelCompletableObserver
import com.comarch.clm.mobileapp.core.presentation.ViewModelObserver
import com.comarch.clm.mobileapp.core.presentation.ViewModelSingleObserver
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.realm.RealmList
import java.util.concurrent.TimeUnit.MILLISECONDS

class ScanProductViewModel(
    var app: Application) : BaseViewModel<ScanProductViewState>(
    app), AccrualContract.ScanProductViewModel {

  private val useCase = injector(app).instance<AccrualContract.AccrualUseCase>()


  init {
    getProducts()
  }

  override fun addProduct(code: String) {
    setState(getState().copy(scannerEnabled = false))

    useCase.addWizardProduct(code)
        .subscribeWith(ViewModelCompletableObserver(this, true)
        {
          postEvent(AddCodeSuccessResult())
        }).addTo(disposables)
  }

  override fun getProducts() {
    useCase.getAccrualWizardProducts().subscribeWith(ViewModelObserver(this)
    {
      setState(getState().copy(productList = it.sortedByDescending{ it.addedDate}))
    }).addTo(disposables)
  }

  override fun getDefaultViewState(): ScanProductViewState = ScanProductViewState()




}