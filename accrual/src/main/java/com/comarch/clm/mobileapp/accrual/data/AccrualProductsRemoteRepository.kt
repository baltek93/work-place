package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.accrual.data.model.AccrualProduct
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface AccrualProductsRemoteRepository{

  @GET("product")
  fun fetchProduct(
      @Header("Authorization") authorization: String,
      @Query("externalCode") externalCode: String) : Single<AccrualProduct>
}