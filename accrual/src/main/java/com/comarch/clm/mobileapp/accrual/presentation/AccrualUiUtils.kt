package com.comarch.clm.mobileapp.accrual.presentation

import com.comarch.clm.mobileapp.accrual.R.string
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProductItem
import com.comarch.clm.mobileapp.core.util.components.CLMLabel
import com.comarch.clm.mobileapp.core.util.components.styles.CLMLabelStyles

object AccrualUiUtils {

  fun setStatusValue(statusValue: CLMLabel,model: AccrualWizardProduct,foundText:String,notFoundText:String,restrictionText:String) {
    if (model.code != "") {
      if (model.restricted) {
        statusValue.text = restrictionText
        statusValue.setStyle(CLMLabelStyles.TEXT_FIELD_LABEL_ON_SURFACE_ERROR)
      } else {
        statusValue.text = foundText
        statusValue.setStyle(CLMLabelStyles.HEADER_3_ON_SURFACE_SECONDARY)
      }
    } else {
      statusValue.text = notFoundText
      statusValue.setStyle(CLMLabelStyles.HEADER_3_ON_SURFACE_ERROR)
    }
  }

}