package com.comarch.clm.mobileapp.accrual.presentation.scan

import android.content.Context
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddCodeSuccessResult
import com.comarch.clm.mobileapp.accrual.AccrualContract.ScanProductViewState
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.BasePresenter
import com.comarch.clm.mobileapp.core.presentation.SuccessResult
import com.comarch.clm.mobileapp.core.presentation.ViewModelSingleObserver
import com.comarch.clm.mobileapp.media.MediaContract
import com.comarch.clm.mobileapp.media.MediaContract.MediaProvider.CameraListener
import com.comarch.clm.mobileapp.media.scanner.ScannerDetector
import com.comarch.clm.mobileapp.media.scanner.ScannerDetector.DetectorListener
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit.MILLISECONDS

class ScanProductPresenter(
    val view: AccrualContract.ScanProductView,
    val router: Architecture.Router,
    viewModel: AccrualContract.ScanProductViewModel,
    uiEventHandler: Architecture.UiEventHandler,
    val mediaProvider: MediaContract.MediaProvider,
    val callbacksHandler: Architecture.CLMCallbacksHandler
) : BasePresenter<ScanProductViewState, AccrualContract.ScanProductViewModel>(
    viewModel,
    uiEventHandler
), AccrualContract.ScanProductPresenter, DetectorListener, CameraListener {

  var isScannerEnabled = true

  init {
    view.onDonePressed().subscribeBy {
      router.previousScreen()
    }.addTo(disposables)
  }

  var detector: ScannerDetector? = null

  @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
  fun onCreate() {
    callbacksHandler.addCallback(mediaProvider)
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
  fun onDestroy() {
    detector?.closeDetector()
    detector = null
    view.getCameraViewFromView().destroy()
    callbacksHandler.removeCallback(mediaProvider)
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
  fun onResume() {
    openScanner()
  }

  @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
  fun onPause() {
    view.getCameraViewFromView().close()
  }

  override fun openScanner() {
    detector = ScannerDetector.getInstance(view.getCameraViewFromView(), this, BitmapFactory.decodeResource(view.getContext().resources, R.drawable.barcode_mask))
    showPermissionIfNeededOrShowScanner()
  }

  private fun showPermissionIfNeededOrShowScanner() {
    when {
      mediaProvider.isCameraPermissionGranted() -> {
        view.openScanner(detector!!)
      }
      else -> {
        showCameraPermissionView()
      }
    }
  }

  private fun showCameraPermissionView() {
    mediaProvider.requestCameraPermission(this)
  }

  override fun onViewStateChanged(state: ScanProductViewState) {
    view.render(state)
  }

  override fun onScan(code: String) {
    if (isScannerEnabled) {
      val v = (view.getContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator)
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        v.vibrate(VibrationEffect.createOneShot(200,
            VibrationEffect.DEFAULT_AMPLITUDE))
      }
      else {
        v.vibrate(200)
      }
      viewModel.addProduct(code)
      isScannerEnabled = false
      enableScanner(1500)

    }
//    view.showToast(code)
  }

  private fun enableScanner(delay: Long) {
    Single.just(this).delay(delay, MILLISECONDS).subscribeOn(
        AndroidSchedulers.mainThread()).subscribeBy {
      isScannerEnabled = true
    }
  }

  override fun handleSuccessEvent(successResult: SuccessResult) {
    if (successResult is AddCodeSuccessResult) {
      viewModel.getProducts()
    }
    super.handleSuccessEvent(successResult)
  }

  override fun onCameraPermissionGranted() {
    view.openScanner(detector!!)
  }

  override fun onCameraPermissionDenied() {
  }
}