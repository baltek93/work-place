package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProduct
import com.comarch.clm.mobileapp.accrual.data.model.AccrualRequest
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponse
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.InvoiceFile
import com.comarch.clm.mobileapp.accrual.data.model.realm.InvoiceFileImpl
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.LocalRepository
import com.comarch.clm.mobileapp.core.data.model.Parameter
import com.comarch.clm.mobileapp.core.data.repository.filter.Predicate
import com.comarch.clm.mobileapp.core.util.ClmOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody.Part

class AccrualRepository(
    private val localRepository: LocalRepository,
    private val remoteProductRepository: AccrualProductsRemoteRepository,
    private val remoteTransactionRepository: AccrualTransactionsRemoteRepository,
    private val synchronizationRepository: AccrualSynchronizationRemoteRepository,
    private val tokenRepository: Architecture.TokenRepository

) : AccrualContract.AccrualRepository, LocalRepository by localRepository {


  override fun getAccrualWizardProducts(
      predicate: Predicate?): Observable<List<AccrualWizardProduct>> {

    return localRepository.getObservableList(
        AccrualWizardProduct::class.java, predicate)
  }

  override fun getAccrualWizardProductList(): List<AccrualWizardProduct> {
    return localRepository.getList(
        AccrualWizardProduct::class.java)
  }

  override fun fetchProduct(externalCode: String): Single<AccrualProduct> {
    return tokenRepository.getToken()
        .observeOn(Schedulers.io())
        .flatMap {
          remoteProductRepository.fetchProduct(it.token, externalCode = externalCode)
              .observeOn(AndroidSchedulers.mainThread())
        }
  }

  override fun fetchParameter(code: String): Single<Parameter> {
    return tokenRepository.getTokenOrGuest()
        .observeOn(Schedulers.io())
        .flatMap { it ->
          synchronizationRepository.fetchParameter(it.token, code)
              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .map { it }
        }
  }

  override fun uploadInvoice(avatar: Part): Single<String> {
    return tokenRepository.getToken()
        .observeOn(Schedulers.io()).flatMap {
          remoteTransactionRepository.importInvoice(it.token, avatar)
//              .subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .map {
                it.fileId
              }
        }
  }

  override fun saveInvoiceFile(path: String?, numberInvoice: String?, type: String?): Completable {
    return Completable.fromCallable {
      val localFile = localRepository.get(InvoiceFile::class.java)
      if (localFile == null) {
        localRepository.save(
            InvoiceFileImpl(path = path?.replaceFirst("root/", "") ?: "", invoiceNo = numberInvoice,
                type = type))
      } else {

        localRepository.executeTransaction {
          if (path != null) {
            localFile.path = path.replaceFirst("root/", "")

          }
          if (numberInvoice != null) {
            localFile.invoiceNo = numberInvoice
          }
          if (type != null) {
            localFile.type = type
          }
          localRepository.save(localFile)
        }
      }
    }
  }

  override fun deleteInvoiceFile(): Completable {
    return Completable.fromCallable {
      localRepository.deleteAll(InvoiceFile::class.java)
    }
  }

  override fun getLocalFileInvoice(): Observable<ClmOptional<InvoiceFile?>> {
    return localRepository.getObservable(InvoiceFile::class.java)
  }

  override fun makeSale(accrualRequest: AccrualRequest,
      simulate: Boolean): Single<AccrualResponse> {
    return tokenRepository.getToken()
        .observeOn(Schedulers.io())
        .flatMap {
          remoteTransactionRepository.makeSale(it.token, accrualRequest, simulate)
              .observeOn(AndroidSchedulers.mainThread())
        }
  }

  override fun deleteWizardProduct(code: String): Completable {
    return Completable.fromCallable {
      val wizardProduct = localRepository.getByCode(AccrualWizardProduct::class.java, code)
      localRepository.delete(wizardProduct)
    }
  }

  override fun deleteWizardProductsAndInvoiceFile() {
    localRepository.deleteAll(AccrualWizardProduct::class.java)
//    localRepository.deleteAll(InvoiceFile::class.java)
  }
}

