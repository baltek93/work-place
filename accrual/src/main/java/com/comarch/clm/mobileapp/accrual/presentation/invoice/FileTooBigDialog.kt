package com.comarch.clm.mobileapp.accrual.presentation.invoice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.core.Architecture.Router
import com.comarch.clm.mobileapp.core.presentation.dialog.CLMDialog
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.dialog_file_too_big.view.addAnotherFileButton
import kotlinx.android.synthetic.main.dialog_file_too_big.view.cancelButton

class FileTooBigDialog : CLMDialog() {

  private lateinit var listener: OnBigTooFile
  override val isRoundDialog: Boolean = true
  private lateinit var dialogView: View

  override fun getLayout(): View {
    dialogView = activity?.layoutInflater?.inflate(R.layout.dialog_file_too_big, null)!!
    dialogView.addAnotherFileButton.setOnClickListener {
      dismiss()
      listener.onAddFile()
    }
    dialogView.cancelButton.setOnClickListener {
      dismiss()
    }
    return dialogView
  }

  companion object {


    fun newInstance(listener : OnBigTooFile): FileTooBigDialog {
      val dialog = FileTooBigDialog()
      dialog.listener = listener

      return dialog
    }
  }


  interface OnBigTooFile {
    fun onAddFile()
  }
}
