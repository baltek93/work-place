package com.comarch.clm.mobileapp.accrual.presentation

import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.MakeSaleErrorResult
import com.comarch.clm.mobileapp.accrual.AccrualContract.MakeSaleSuccessResult
import com.comarch.clm.mobileapp.accrual.AccrualContract.SummaryProductListViewModel
import com.comarch.clm.mobileapp.accrual.AccrualContract.SummaryProductListViewState
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProductPoint
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualProductPointImpl
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.BasePresenter
import com.comarch.clm.mobileapp.core.presentation.ErrorResult
import com.comarch.clm.mobileapp.core.presentation.SuccessResult
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class SummaryProductListPresenter(
    val view: AccrualContract.SummaryProductListView,
    val router: Architecture.Router,
    viewModel: SummaryProductListViewModel,
    uiEventHandler: Architecture.UiEventHandler
) : BasePresenter<SummaryProductListViewState, SummaryProductListViewModel>(
    viewModel,
    uiEventHandler
), AccrualContract.SummaryProductListPresenter {

  init {
    view.onPressedRegister().subscribeBy {
      viewModel.registerTransaction()
    }.addTo(disposables)
  }

  override fun onViewStateChanged(state: SummaryProductListViewState) {
    view.render(state)
  }

  override fun handleSuccessEvent(successResult: SuccessResult) {
    if (successResult is MakeSaleSuccessResult) {
      router.nextScreen(MakeSaleSuccessResult(successResult.pointsList))
    }
    super.handleSuccessEvent(successResult)
  }

  override fun handleErrorEvent(errorResult: ErrorResult) {
    if (errorResult is MakeSaleErrorResult) {
      viewModel.errorMakeSale()
    }
    super.handleErrorEvent(errorResult)
  }

  override fun onEditPressed() {
    router.previousScreen()
  }
}