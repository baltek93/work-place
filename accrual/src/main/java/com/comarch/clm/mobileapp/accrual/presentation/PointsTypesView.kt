package com.comarch.clm.mobileapp.accrual.presentation

import android.content.Context
import android.util.AttributeSet
import android.util.LayoutDirection
import android.view.Gravity
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.DividerItemDecoration
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.R
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProductPoint
import com.comarch.clm.mobileapp.accrual.data.model.AccrualWizardProduct
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualProductPointImpl
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.CLMListView.CLMListMode
import com.comarch.clm.mobileapp.core.Architecture.CLMListView.CLMListMode.GRID
import com.comarch.clm.mobileapp.core.Architecture.CLMListView.CLMListMode.LINEAR
import com.comarch.clm.mobileapp.core.Architecture.CLMListView.ViewType.Companion.ITEM_TYPE
import com.comarch.clm.mobileapp.core.data.model.BasePoint
import com.comarch.clm.mobileapp.core.util.components.CLMConstraintLayout
import com.comarch.clm.mobileapp.core.util.components.resources.CLMResourcesResolver
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import kotlinx.android.synthetic.main.item_list_grid.view.cardView
import kotlinx.android.synthetic.main.item_list_grid.view.itemView
import kotlinx.android.synthetic.main.item_list_grid.view.pointsType
import kotlinx.android.synthetic.main.item_list_grid.view.pointsValue
import kotlinx.android.synthetic.main.item_list_grid.view.viewPlus
import kotlinx.android.synthetic.main.item_list_vertical.view.pointsTypeAndValue
import kotlinx.android.synthetic.main.view_points_type.view.pointsTypesLabel
import kotlinx.android.synthetic.main.view_points_type.view.pointsTypesList
import kotlinx.android.synthetic.main.view_points_type.view.rootCardView

class PointsTypesView @kotlin.jvm.JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CLMConstraintLayout(context, attrs, defStyleAttr) {

  data class PointsTypeObject(
      val pointsList: List<BasePoint>? = null,
      val extraPointsList: List<BasePoint>? = null,
      val labelText: String? = null,
      val isGrid: Boolean = false,
      val isDialog: Boolean = false,
      val isWithPointsType: Boolean = true,
      val isWithBackground: Boolean = true)

  var clmResourcesResolver: CLMResourcesResolver = injector(context).instance()
  val pointTypes: List<String> = injector(context).instance(
      tag = AccrualContract.ACCRUAL_POINT_TYPES)

  fun render(item: PointsTypeObject) {
    if (item.pointsList != null)
      if (item.isGrid)
        renderGridLayout(item.pointsList, item.isWithPointsType, item.isWithBackground)
      else
        renderListLayout(item.pointsList)

    if (item.isDialog) {
      renderColorForDialog()
    }

    item.labelText?.let {
      pointsTypesLabel.text = it
    } ?: run {
      pointsTypesLabel.visibility = View.GONE
    }
  }

  private fun renderColorForDialog() {
    rootCardView.setCardBackgroundColor(getTransparentColorForPointTypeBackground(
        clmResourcesResolver.resolveColor(R.string.styles_color_primary)))
  }


  private fun sumUpPointsAndRenderSummary(productList: List<AccrualWizardProduct>, isGrid: Boolean,
      withPointsType: Boolean, withBackground: Boolean) {
    val pointList = arrayListOf<AccrualProductPoint>()
    val returnPointList = arrayListOf<AccrualProductPoint>()
    productList.forEach { pointList.addAll(it.points) }
    val pointsHash = pointList.groupBy { it.pointTypeName }
    pointsHash.forEach { (s, list) ->
      returnPointList.add(AccrualProductPointImpl(pointTypeName = s, pointType = list[0].pointType,
          points = list.sumByLong { it.points }))
    }
    if (isGrid)
      renderGridLayout(returnPointList, withPointsType, withBackground)
    else
      renderListLayout(returnPointList)
  }

  private fun setListMode(listMode: CLMListMode) {
    if (pointsTypesList.getListMode() != listMode) {
      pointsTypesList.toggleListMode()
    }
  }

  private fun initializeClmListView() {
    pointsTypesList.setViewTypes(listOf(
        Architecture.CLMListView.ViewType(ITEM_LAST_IN_ROW, gridLayoutId = R.layout.item_list_grid_last),
        Architecture.CLMListView.ViewType(ITEM_NORMAL, gridLayoutId = R.layout.item_list_grid),
        Architecture.CLMListView.ViewType(ITEM_LINEAR, R.layout.item_list_vertical)
    ))
    pointsTypesList.setGridColumns(3)
  }


  private fun renderGridLayout(item: List<BasePoint>, isWithPointsType: Boolean,
      isWithBackground: Boolean) {
    setListMode(GRID)
    pointsTypesList.setViewLayoutDirection(LayoutDirection.RTL)
    val divider = DividerItemDecoration(pointsTypesList.context, DividerItemDecoration.HORIZONTAL)
    divider.setDrawable(
        ContextCompat.getDrawable(pointsTypesList.context, R.drawable.plus_divider)!!)
    pointsTypesList.addItemDecoration(divider)

    pointsTypesList.render(object : Architecture.CLMListView.Renderable {
      override val size: Int
        get() = item.size

      override fun bindView(view: View, position: Int) {
        val itemPoint = item[position]

        view.pointsValue.text = itemPoint.points.toString()
        view.pointsType.text = itemPoint.pointTypeName

        if (isWithPointsType) {
          view.pointsType.visibility = View.VISIBLE
        } else {
          view.pointsType.visibility = View.GONE
        }

        view.pointsValue.setLabelColor(getColorForColorPointType(itemPoint.pointType))
        view.pointsType.setLabelColor(getColorForColorPointType(itemPoint.pointType))

        if (isWithBackground) {
          view.cardView.setCardBackgroundColor(getTransparentColorForPointTypeBackground(
              getColorForColorPointType(itemPoint.pointType)))
        } else {
          if (view.cardView.minimumWidth != 0) {
            view.cardView.minimumWidth = 0
            view.cardView.invalidate()
          }
        }
      }

      override fun getViewTypeForPos(position: Int): Int {
        return if(position == size || position % 3 == 0)
          ITEM_LAST_IN_ROW
        else
          ITEM_NORMAL
      }
    })
  }

  private fun renderListLayout(item: List<BasePoint>) {
    setListMode(LINEAR)
    val divider = DividerItemDecoration(pointsTypesList.context, DividerItemDecoration.VERTICAL)
    divider.setDrawable(
        ContextCompat.getDrawable(pointsTypesList.context, R.drawable.plus_divider)!!)
    pointsTypesList.addItemDecoration(divider)
    pointsTypesList.render(object : Architecture.CLMListView.Renderable {
      override val size: Int
        get() = item.size

      override fun bindView(view: View, position: Int) {
        val itemPoint = item[position]
        view.pointsTypeAndValue.text = "${itemPoint.points} ${itemPoint.pointTypeName}"
        view.pointsTypeAndValue.setLabelColor(getColorForColorPointType(itemPoint.pointType))
      }

      override fun getViewTypeForPos(position: Int): Int {
        return ITEM_LINEAR
      }
    })
  }

  private fun getTransparentColorForPointTypeBackground(color: Int): Int {
    return ColorUtils.setAlphaComponent(color, 50)
  }


  private fun getColorForColorPointType(pointType: String): Int {
    return when (pointTypes.indexOfFirst { pointType.contains(it) }) {
      0 -> clmResourcesResolver.resolveColor(R.string.styles_color_primary)
      1 -> clmResourcesResolver.resolveColor(R.string.styles_color_accent)
      2 -> clmResourcesResolver.resolveColor(R.string.styles_color_primary_variant)
      3 -> clmResourcesResolver.resolveColor(R.string.styles_color_accent_variant)
      else -> clmResourcesResolver.resolveColor(R.string.styles_color_primary)
    }
  }

  init {
    inflate(context, R.layout.view_points_type, this)
    initializeClmListView()
  }

  companion object {
    val ITEM_LAST_IN_ROW = 10
    val ITEM_NORMAL = 11
    val ITEM_LINEAR = 12
  }

}


inline fun <T> Iterable<T>.sumByLong(selector: (T) -> Long): Long {
  var sum = 0L
  for (element in this) {
    sum += selector(element)
  }
  return sum
}