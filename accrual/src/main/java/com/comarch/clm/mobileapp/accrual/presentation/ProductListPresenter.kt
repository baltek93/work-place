package com.comarch.clm.mobileapp.accrual.presentation

import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.ADD_INVOICE_ROUTE
import com.comarch.clm.mobileapp.accrual.AccrualContract.ADD_PRODUCT_CODE_ROUTE
import com.comarch.clm.mobileapp.accrual.AccrualContract.ADD_SCANNER_PRODUCT_CODE_ROUTE
import com.comarch.clm.mobileapp.accrual.AccrualContract.ProductListViewState
import com.comarch.clm.mobileapp.accrual.AccrualContract.SUMMARY_PRODUCT_LIST_ROUTE
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.BasePresenter
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class ProductListPresenter(
    val view: AccrualContract.ProductListView,
    val router: Architecture.Router,
    viewModel: AccrualContract.ProductListViewModel,
    uiEventHandler: Architecture.UiEventHandler
) : BasePresenter<ProductListViewState, AccrualContract.ProductListViewModel>(
    viewModel,
    uiEventHandler
), AccrualContract.ProductListPresenter {

  var state = ProductListViewState()

  init {
    view.onPressedAddCode().subscribeBy {
      router.nextScreen(ADD_PRODUCT_CODE_ROUTE)
    }.addTo(disposables)

    view.onPressedScan().subscribeBy {
      onPressedScan()
    }.addTo(disposables)

  }

  override fun onPressedScan() {
    router.nextScreen(ADD_SCANNER_PRODUCT_CODE_ROUTE)
  }

  override fun goToNextStep() {
    if (state.isAddInvoice) {
      router.nextScreen(ADD_INVOICE_ROUTE)
    } else {
      router.nextScreen(SUMMARY_PRODUCT_LIST_ROUTE)
    }
  }

  override fun onDeleteClicked(id: String) {
    viewModel.onDeleteClicked(id)
  }


  override fun onViewStateChanged(state: ProductListViewState) {
    this.state = state
    view.render(state)
  }
}