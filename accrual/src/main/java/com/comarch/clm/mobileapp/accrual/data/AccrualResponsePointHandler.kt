package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponsePoint
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualResponsePointImpl
import com.comarch.clm.mobileapp.core.data.model.ModelHandler
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class AccrualResponsePointHandler : ModelHandler<AccrualResponsePoint> {

  override fun instantiate(): AccrualResponsePoint = AccrualResponsePointImpl()

  override fun getClassMappings(): Map<Class<*>, Class<*>> {
    return mapOf(
        AccrualResponsePoint::class.java to AccrualResponsePointImpl::class.java
    )
  }

  @FromJson
  fun toInterface(modelImpl: AccrualResponsePointImpl): AccrualResponsePoint {
    return modelImpl
  }

  @ToJson
  fun toImplementation(modelIf: AccrualResponsePoint): AccrualResponsePointImpl {
    return modelIf as AccrualResponsePointImpl
  }
}