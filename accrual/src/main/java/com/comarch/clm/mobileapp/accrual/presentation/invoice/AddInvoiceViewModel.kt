package com.comarch.clm.mobileapp.accrual.presentation.invoice

import android.app.Application
import android.net.Uri
import android.webkit.MimeTypeMap
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.AddInvoiceViewState
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel
import com.comarch.clm.mobileapp.core.presentation.ViewModelCompletableObserver
import com.comarch.clm.mobileapp.core.presentation.ViewModelObserver
import com.comarch.clm.mobileapp.core.presentation.ViewModelSingleObserver
import com.comarch.clm.mobileapp.core.util.ClmLogger
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import io.reactivex.rxkotlin.addTo
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class AddInvoiceViewModel(
    var app: Application) : BaseViewModel<AddInvoiceViewState>(
    app), AccrualContract.AddInvoiceViewModel {
  private val useCase = injector(app).instance<AccrualContract.AccrualUseCase>()


  override fun getDefaultViewState(): AddInvoiceViewState = AddInvoiceViewState()
  override fun saveInvoiceUri(uri: Uri, type: String) {
    useCase.saveInvoiceFile(UriUtils.getPathFromUri(app, uri), null, type).subscribeWith(
        ViewModelCompletableObserver(this)).addTo(
        disposables)
  }

  override fun deleteInvoiceFile() {
    useCase.deleteInvoiceFile().subscribeWith(ViewModelCompletableObserver(this)).addTo(disposables)
  }

  override fun saveInvoiceNumber(it: String?) {
    useCase.saveInvoiceFile(null, it).subscribeWith(ViewModelCompletableObserver(this)).addTo(
        disposables)
  }

  override fun changeInvoiceNumber(it: String?) {
    useCase.saveInvoiceFile(null, it).subscribeWith(ViewModelCompletableObserver(this)).addTo(
        disposables)
  }

  override fun uploadFile() {
    if (getState().invoiceUri != null) {
      useCase.uploadInvoice(convertFileToMultipart(getState().invoiceUri!!)).subscribeWith(
          ViewModelSingleObserver(this)
          {
            ClmLogger.log("PENIS DENIS " + it)
          }).addTo(disposables)

    }
  }

  init {
    useCase.getParameter(AccrualContract.ACCRUAL_TRN_CONFIRMATION_FILE_MAX_SIZE)
        .subscribeWith(ViewModelObserver(this) {
          setState(getState().copy(maxSizeFile = it.value?.value?.toInt() ?: 5))
        })
        .addTo(disposables)
    useCase.getLocalObservableFileInvoice().subscribeWith(ViewModelObserver(this)
    {
      if (it.value != null) {
        val file = File(it?.value?.path)
        val ls = file.length().toFloat() / (1024 * 1024)

        val isFileTooBig = if (getState().maxSizeFile > 0) {
          getState().maxSizeFile < ls
        } else {
          false
        }
        setState(getState().copy(isAttachmentAttached = !it?.value?.path.isNullOrBlank(), invoiceUri = it?.value?.path,
            isFileTooBig = isFileTooBig, invoiceNumber = it.value?.invoiceNo))
      } else {
        setState(getState().copy(isAttachmentAttached = false))
      }
    }).addTo(disposables)
  }

  private fun convertFileToMultipart(uri: String): MultipartBody.Part {
    val file = File(uri)

    val fileName = file.name
    val requestBody = RequestBody.create(MediaType.parse(getMimeType(uri)), file)
    val avatar = MultipartBody.Part.createFormData("file", file.name,
        requestBody)//
    return avatar as MultipartBody.Part
  }
}

fun getMimeType(url: String?): String? {
  var type: String? = null
  val extension: String = MimeTypeMap.getFileExtensionFromUrl(url)
  if (extension != null) {
    type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
  }
  return type
}
