package com.comarch.clm.mobileapp.accrual.data

import com.comarch.clm.mobileapp.accrual.data.model.InvoiceFile
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProduct
import com.comarch.clm.mobileapp.accrual.data.model.realm.InvoiceFileImpl
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualProductImpl
import com.comarch.clm.mobileapp.core.data.model.ModelHandler
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class AccrualProductHandler : ModelHandler<AccrualProduct> {

  override fun instantiate(): AccrualProduct = AccrualProductImpl()

  override fun getClassMappings(): Map<Class<*>, Class<*>> {
    return mapOf(
        AccrualProduct::class.java to AccrualProductImpl::class.java,
        InvoiceFile::class.java to InvoiceFileImpl::class.java
    )
  }

  @FromJson
  fun toInterface(modelImpl: AccrualProductImpl): AccrualProduct {
    return modelImpl
  }

  @ToJson
  fun toImplementation(modelIf: AccrualProduct): AccrualProductImpl {
    return modelIf as AccrualProductImpl
  }
}