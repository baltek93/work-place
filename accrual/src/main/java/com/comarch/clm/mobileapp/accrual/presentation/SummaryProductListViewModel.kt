package com.comarch.clm.mobileapp.accrual.presentation

import android.app.Application
import android.net.Uri
import android.webkit.MimeTypeMap
import com.comarch.clm.mobileapp.accrual.AccrualContract
import com.comarch.clm.mobileapp.accrual.AccrualContract.MakeSaleErrorResult
import com.comarch.clm.mobileapp.accrual.AccrualContract.MakeSaleSuccessResult
import com.comarch.clm.mobileapp.accrual.AccrualContract.SummaryProductListViewState
import com.comarch.clm.mobileapp.accrual.data.model.AccrualProductPoint
import com.comarch.clm.mobileapp.accrual.data.model.AccrualResponse
import com.comarch.clm.mobileapp.accrual.data.model.InvoiceFile
import com.comarch.clm.mobileapp.accrual.data.model.realm.AccrualProductPointImpl
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel
import com.comarch.clm.mobileapp.core.presentation.ViewModelCompletableObserver
import com.comarch.clm.mobileapp.core.presentation.ViewModelObserver
import com.comarch.clm.mobileapp.core.presentation.ViewModelSingleObserver
import com.comarch.clm.mobileapp.core.util.components.CLMProgressButton.State.ACTIVE
import com.comarch.clm.mobileapp.core.util.components.CLMProgressButton.State.PROGRESS
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import io.reactivex.rxkotlin.addTo
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class SummaryProductListViewModel(
    var app: Application) : BaseViewModel<SummaryProductListViewState>(
    app), AccrualContract.SummaryProductListViewModel {

  private val useCase = injector(app).instance<AccrualContract.AccrualUseCase>()

  override fun getDefaultViewState(): SummaryProductListViewState = SummaryProductListViewState()

  init {
    useCase.getParameter(AccrualContract.ACCRUAL_TRN_CONFIRMATION_ENABLED).map {
      it.value?.value == "true"
    }
        .subscribeWith(ViewModelObserver(this) {
          setState(getState().copy(isAddInvoice = it))
        })
        .addTo(disposables)

    useCase.getLocalObservableFileInvoice().subscribeWith(ViewModelObserver(this)
    {
      setState(getState().copy(fileAttachment = it?.value))
    }).addTo(disposables)

    useCase.getAccrualWizardProducts(isFoundProduct = true).subscribeWith(ViewModelObserver(this)
    { products ->
      val result = mutableListOf<AccrualProductPoint>()

      products.forEach { product ->
        product.points.forEach { point ->
          val currentPoint = result.firstOrNull { it.pointType == point.pointType }
          if (currentPoint != null) {
            currentPoint.points += point.points
          } else {
            result.add(AccrualProductPointImpl(
                points = point.points,
                pointType = point.pointType,
                pointTypeName = point.pointTypeName,
            ))
          }
        }
      }


      setState(getState().copy(productList = products, totalPoints = result))
      calculateSummary()
    }).addTo(disposables)
  }


  private fun calculateSummary() {
    useCase.makeAccrual(useCase.buildRequest(getState().productList, getState().fileAttachment),
        true).subscribeWith(
        ViewModelSingleObserver(this)
        {
          val totalPoints = getState().totalPoints
          val extraPoints = calculateExtraPoints(totalPoints, it)
          setState(getState().copy(extraPoints = extraPoints))
        }).addTo(disposables)
  }

  override fun registerTransaction() {
    val filePart = if (getState().fileAttachment?.path != null) {
      getState().fileAttachment?.let { convertFileToMultipart(it) }
    } else {
      null
    }
    setState(getState().copy(stateButton = PROGRESS))
    useCase.makeAccrual(useCase.buildRequest(getState().productList, getState().fileAttachment),
        false,
        filePart).doOnError {
      postEvent(MakeSaleErrorResult(it))
      it
    }.subscribeWith(
        ViewModelSingleObserver(this)
        {
          setState(getState().copy(stateButton = ACTIVE))
          postEvent(MakeSaleSuccessResult(it.transactionPoints?.points))
        }).addTo(disposables)
  }

  override fun deleteFileFromDataBase() {
    useCase.deleteInvoiceFile().subscribeWith(ViewModelCompletableObserver(this)).addTo(disposables)
  }

  override fun errorMakeSale() {
    setState(getState().copy(stateButton = ACTIVE))
  }


  private fun calculateExtraPoints(totalPoints: List<AccrualProductPoint>,
      response: AccrualResponse): List<AccrualProductPoint> {
    return response.transactionPoints?.points?.map { points ->

      val totalPoint = totalPoints.firstOrNull { it.pointType == points.pointType }
      val point = if (totalPoint != null) {
        points.points - totalPoint.points
      } else {
        points.points
      }
      val newPoint = AccrualProductPointImpl(
          pointType = points.pointType,
          pointTypeName = points.pointTypeName,
          points = point
      )
      newPoint
    } ?: emptyList()


  }

  private fun convertFileToMultipart(invoiceFile: InvoiceFile): MultipartBody.Part {


    val file = File(invoiceFile.path!!)

    val fileName = file.name
    val requestBody = RequestBody.create(MediaType.parse(getMimeType(invoiceFile.path)), file)
    val avatar = MultipartBody.Part.createFormData("file", file.name,
        requestBody)//
    return avatar as MultipartBody.Part
  }

  fun getFileName(uri: Uri?): String? {
    if (uri == null) return null
    var fileName: String? = null
    val path = uri.path
    val cut = path!!.lastIndexOf('/')
    if (cut != -1) {
      fileName = path.substring(cut + 1)
    }
    return fileName
  }

  fun getMimeType(url: String?): String? {
    var type: String? = null
    val extension: String = MimeTypeMap.getFileExtensionFromUrl(url)
    if (extension != null) {
      type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    }
    return type
  }


}