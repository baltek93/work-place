package com.comarch.clm.mobileapp.content.survey.presentation

import android.content.DialogInterface
import android.os.Bundle
import android.util.LayoutDirection
import androidx.annotation.StringRes
import android.view.View
import com.comarch.clm.mobileapp.content.R
import com.comarch.clm.mobileapp.core.presentation.dialog.CLMDialog
import kotlinx.android.synthetic.main.dialog_survey.view.*

class SurveyDialog : CLMDialog() {
    @StringRes
    override fun dialogCancelText() = 0

    override val isRoundDialog: Boolean = true

    companion object {
        val BUNDLE_SURVEY_NAME = SurveyDialog.javaClass.canonicalName + "NAME"
        val BUNDLE_SURVEY_ID = SurveyDialog.javaClass.canonicalName + "ID"
        val BUNDLE_SURVEY_DESCRIPTION = SurveyDialog.javaClass.canonicalName + "DESCRIPTION"
        fun getInstance(
                id: String,
                name: String,
                description: String,
                listener: Listener
        ): SurveyDialog {
            return SurveyDialog().apply {
                this.listener = listener
                this.arguments = Bundle().apply {
                    putString(BUNDLE_SURVEY_NAME, name)
                    putString(BUNDLE_SURVEY_ID, id)
                    putString(BUNDLE_SURVEY_DESCRIPTION, description)
                }
            }
        }
    }

    var listener: Listener? = null
    lateinit var dialogView: View
    override fun getLayout(): View {
        dialogView = activity?.layoutInflater?.inflate(R.layout.dialog_survey, null)!!
        dialogView.dialogSurveyName.text = arguments?.getString(BUNDLE_SURVEY_NAME, "")
        dialogView.dialogSurveyDescription.text = arguments?.getString(BUNDLE_SURVEY_DESCRIPTION, "")
        dialogView.dialogSurveyStartButton.setOnClickListener {
            dismiss()
            listener?.navigateToDetailsSurveys(arguments?.getString(BUNDLE_SURVEY_ID, "") as String
            )
        }
      dialogView.dialogSurveyCancelButton.setOnClickListener {
            dismiss()
        }
        return dialogView
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
//    listener?.backToListSurvey()
    }

    interface Listener {
        fun navigateToDetailsSurveys(id: String)
    }
}
