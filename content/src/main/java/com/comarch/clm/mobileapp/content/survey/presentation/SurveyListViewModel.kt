package com.comarch.clm.mobileapp.content.survey.presentation

import android.app.Application
import com.comarch.clm.mobileapp.content.ContentContract
import com.comarch.clm.mobileapp.content.ContentContract.SurveyListViewState
import com.comarch.clm.mobileapp.content.ContentContract.SurveyUseCase
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel
import com.comarch.clm.mobileapp.core.util.ClmLogger
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import io.reactivex.CompletableObserver
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver

class SurveyListViewModel(app: Application) : BaseViewModel<SurveyListViewState>(app),
    ContentContract.SurveyListViewModel {

  interface Observers {
    interface UpdateSurveysObserver : CompletableObserver, Disposable
    interface GetSurveysObserver : Observer<List<Survey>>, Disposable
  }

  private val updateSurveysObserver: Observers.UpdateSurveysObserver = injector(app).instance()
  private var getSurveysObserver: Observers.GetSurveysObserver = injector(app).with(this)
      .instance()
  private val useCase = injector(app).instance<SurveyUseCase>()

  override fun getDefaultViewState(): SurveyListViewState = SurveyListViewState()

  init {
    setState(getState().copy())
    addToDisposables(useCase.updateSurveys().subscribeWith(updateSurveysObserver))
    addToDisposables(
        useCase.getSurveys().subscribeWith(getSurveysObserver)
    )
  }

  inner class GetSurveyObserver : Observers.GetSurveysObserver,
      DisposableObserver<List<Survey>>() {
    override fun onComplete() {
      ClmLogger.log("get surveys completed")
    }

    override fun onNext(t: List<Survey>) {
      setState(getState().copy(surveys = t))
    }

    override fun onError(e: Throwable) {
      ClmLogger.log(e.message!!)
    }

  }

  class UpdateSurveysObserver : Observers.UpdateSurveysObserver, DisposableCompletableObserver() {
    override fun onComplete() {
      ClmLogger.log("update surveys completed")
    }

    override fun onError(e: Throwable) {
      ClmLogger.log(e.message!!)
    }

  }

}