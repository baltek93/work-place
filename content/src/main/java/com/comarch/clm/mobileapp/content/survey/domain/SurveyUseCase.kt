package com.comarch.clm.mobileapp.content.survey.domain

import com.comarch.clm.mobileapp.content.ContentContract
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyRequest
import com.comarch.clm.mobileapp.core.ApplicationContract
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.DataSynchronizationContract
import com.comarch.clm.mobileapp.core.data.repository.filter.Order.ASCENDING
import com.comarch.clm.mobileapp.core.data.repository.filter.PredicateFactory
import com.comarch.clm.mobileapp.core.domain.UseCase
import com.comarch.clm.mobileapp.core.domain.filters.ActiveDateRangeFilter
import com.comarch.clm.mobileapp.core.presentation.CLMFilterPredicate
import com.comarch.clm.mobileapp.core.util.ClmOptional
import io.reactivex.Completable
import io.reactivex.Observable

class SurveyUseCase(
    private val errorHandler: Architecture.ErrorHandler,
    schedulerApplier: Architecture.SchedulerApplier,
    private val applicationState: ApplicationContract.ApplicationState,
    private val repository: ContentContract.SurveyRepository,
    private val synchronizationUseCase: DataSynchronizationContract.DataSynchronizationManager,
    private val predicateFactory: PredicateFactory
) : UseCase(schedulerApplier, applicationState), ContentContract.SurveyUseCase {

  override fun sendSurvey(surveyRequest: SurveyRequest): Completable =
      repository.sendSurvey(surveyRequest)
          .compose(errorHandler.handleErrorOnCompletable())
          .compose(schedulerApplier.changeToForeground())
          .doOnComplete {
            repository.deleteSurvey(surveyRequest.code)
          }

          .compose(schedulerApplier.startOnBackgroundCompletable())

  override fun updateSurvey(idSurvey: String): Completable {
    return repository.updateSurvey(idSurvey)
        .compose(errorHandler.handleErrorOnCompletable())
  }

  override fun getSurvey(code: String): Observable<ClmOptional<Survey?>> {
    return repository.getSurvey(code)
  }

  override fun updateSurveys(): Completable =
      repository.updateSurveys()
//          .compose(synchronizationUseCase.checkIfNeeded(Survey::class.java))
          .compose(errorHandler.handleErrorOnCompletable())

  override fun getSurveys(filterPredicate: CLMFilterPredicate?): Observable<List<Survey>> {
    var realmPredicate = predicateFactory.and(
        predicateFactory.equal(
            Survey.FIELD_FILLED_BY_USER, false),
        predicateFactory.`in`(
            Survey.FIELD_STATUS, listOf(Survey.STATUS_ACTIVE, ""))
    )
    realmPredicate = ActiveDateRangeFilter.create(Survey.FIELD_START_DATE, Survey.FIELD_END_DATE,
        realmPredicate, predicateFactory)
    realmPredicate = predicateFactory.sort(realmPredicate, Survey.FIELD_NAME, ASCENDING)
    return repository.getSurveys(globalPredicate = realmPredicate)
  }

}