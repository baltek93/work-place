package com.comarch.clm.mobileapp.content.survey.presentation

import android.app.Application
import com.comarch.clm.mobileapp.content.ContentContract.SurveyDetailsViewModel
import com.comarch.clm.mobileapp.content.ContentContract.SurveyDetailsViewState
import com.comarch.clm.mobileapp.content.ContentContract.SurveyUseCase
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.core.ParametersContract.ParametersUseCase
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel
import com.comarch.clm.mobileapp.core.presentation.ViewModelCompletableObserver
import com.comarch.clm.mobileapp.core.util.ClmLogger
import com.comarch.clm.mobileapp.core.util.ClmOptional
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import io.reactivex.CompletableObserver
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.rxkotlin.addTo

class SurveyDetailsViewModel(
    app: Application,
    surveyID: String
) : BaseViewModel<SurveyDetailsViewState>(app),
    SurveyDetailsViewModel {
  override fun checkSurveyCompleted(): Boolean {
    val surveyRequest = getState().surveyRequest
    val survey = getState().survey
    var isCompletable = true
    survey?.questions?.forEachIndexed { index, surveyQuestion ->
      if (surveyQuestion.required) {
        if (surveyRequest.answers[index]?.answerValue ?: "" == "") {
          isCompletable = true
        }
      }
    }
    return isCompletable
  }

  override fun sendSurvey() {
    var i = getState().surveyRequest.answers.iterator()
    while (i.hasNext()) {
      if (i.next().answerValue == "") {
        i.remove()
      }
    }
    useCase.sendSurvey(getState().surveyRequest).subscribeWith(
        ViewModelCompletableObserver(this, true, onComplete = {
          setState(getState().copy(isFinishSurvey = true))
        })).addTo(disposables)
  }

  override fun getDefaultViewState(): SurveyDetailsViewState = SurveyDetailsViewState()

  interface Observers {
    interface UpdateSurveyObserver : CompletableObserver, Disposable
    interface GetSurveyObserver : Observer<ClmOptional<Survey?>>, Disposable
  }

  private val useCase = injector(app).instance<SurveyUseCase>()
  private val synchronizationUseCase = injector(app).instance<ParametersUseCase>()
  private val updateSurveyObservable: Observers.UpdateSurveyObserver = injector(app).instance()
  private var getSurveysObserver: Observers.GetSurveyObserver = injector(app).with(this)
      .instance()

  init {

    addToDisposables(useCase.updateSurvey(surveyID).subscribeWith(updateSurveyObservable))
    addToDisposables(useCase.getSurvey(surveyID).subscribeWith(getSurveysObserver))

  }

  inner class GetSurveyObserver : Observers.GetSurveyObserver, DisposableObserver<ClmOptional<Survey?>>() {
    override fun onComplete() {
      ClmLogger.log("get surveys completed")

    }

    override fun onNext(t: ClmOptional<Survey?>) {
      if (t.value != null) {
        setState(getState().copy(survey = t.value))
      }
    }

    override fun onError(e: Throwable) {
      ClmLogger.log(e.message!!)
    }

  }

  class UpdateSurveyObserver : Observers.UpdateSurveyObserver, DisposableCompletableObserver() {
    override fun onComplete() {
      ClmLogger.log("update surveys completed")

    }

    override fun onError(e: Throwable) {
      ClmLogger.log(e.message!!)

    }

  }
}