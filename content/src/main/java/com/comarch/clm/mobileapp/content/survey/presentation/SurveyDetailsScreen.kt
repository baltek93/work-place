package com.comarch.clm.mobileapp.content.survey.presentation

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.comarch.clm.mobileapp.content.ContentContract
import com.comarch.clm.mobileapp.content.ContentContract.*
import com.comarch.clm.mobileapp.content.ContentContract.SurveyDetailsPresenter
import com.comarch.clm.mobileapp.content.R
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyQuestion
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyRequest
import com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyAnswer
import com.comarch.clm.mobileapp.core.Architecture.Navigator.NavigationPoint
import com.comarch.clm.mobileapp.core.presentation.BaseFragment
import com.comarch.clm.mobileapp.core.presentation.toolbar.ToolbarContract.State.BACK
import com.comarch.clm.mobileapp.core.util.components.styles.CLMLabelStyles.Companion.HEADER_2_ON_PRIMARY
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import io.realm.RealmList
import kotlinx.android.synthetic.main.screen_survey.view.*

open class SurveyDetailsScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(
    context, attrs,
    defStyleAttr
), SurveyDetailsView {

  lateinit var surveyAdapter: SurveyAdapter
  lateinit var survey: Survey
  lateinit var surveyRequest: SurveyRequest
  var isBack = false
  var isFirstRender = true
  override fun isAnswerCheckedAndRequired(
      questionList: List<SurveyQuestion>,
      answerList: List<com.comarch.clm.mobileapp.content.survey.data.model.SurveyAnswer>
  ) {
    if (questionList[viewPager.currentItem].required && answerList[viewPager.currentItem].answerValue == "") {
      nextButton.alpha = .5f
      nextButton.isEnabled = false
    } else {
      nextButton.alpha = 1f
      nextButton.isEnabled = true
    }

  }

  override fun goToNextPageOrFinishSurvey() {
    if (surveyAdapter.count == viewPager.currentItem + 1) {
      presenter.onCheckSurveyCompleted()
    } else {
      viewPager.setCurrentItem(viewPager.currentItem + 1, true)
    }
  }

  override fun goToBackPage() {
    viewPager.setCurrentItem(viewPager.currentItem - 1, true)
    surveyAdapter.checkIfIsAnswered()
    isBack = true
  }

  override fun clickBackPressed(): Observable<Any> {
    return RxView.clicks(backButton)
  }

  override fun clickNextPressed(): Observable<Any> {
    return RxView.clicks(nextButton)
  }

  override fun render(state: SurveyDetailsViewState) {
    if (state.survey != null && state.survey.questions.size > 0 && isFirstRender) {
      loader.visibility = View.VISIBLE
      nextButton.text = resources.getText(R.string.labels_cma_core_action_next)
      isFirstRender = false
      var isAnswerEmpty = false
      state.survey.questions.forEach {
        if (it.type == "M" || it.type == "L") {
          if (it.answerChoices.size == 0) {
            isAnswerEmpty = true
          }
        }
      }
      if (isAnswerEmpty) {
        backButton.visibility = View.GONE
        nextButton.visibility = View.GONE
        if (!state.isShowDialogEmpty) {
          presenter.showDialogEmptyQuestion()
          state.isShowDialogEmpty = true
        }
      } else {
        if (viewPager.adapter == null && state.survey.questions.size > 0) {
          toolbar.setTitle(state.survey.name)
          toolbar.setScrollabeTitle(true)
        }

        loader.max = (state.survey.questions.size)
        state.surveyRequest.code = state.survey.code
        (state.surveyRequest as com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyRequest).answers =
            emptyAnserwList(survey = state.survey)
        survey = state.survey
        surveyRequest = state.surveyRequest
        initAdapter(state.survey, state)
        viewPager.adapter = surveyAdapter
        loader.progress = (viewPager.currentItem + 1)
        backButton.visibility = View.GONE
        nextButton.visibility = View.VISIBLE
        if (viewPager.currentItem == 0) {
          questionCounterLabel.text = resources.getString(
              R.string.labels_cma_content_survey_question_format, (viewPager.currentItem + 1),
              state.survey.questions.size
          )
        }
        if ((viewPager.currentItem + 1) >= loader.max) {
          nextButton.text = resources.getText(R.string.labels_cma_core_action_send)
          loaderOneQuestion()
        }
        toolbar.setTitleSmall(state.survey.name)
        resources.getString(
            R.string.labels_cma_content_survey_question_format, (viewPager.currentItem + 1),
            state.survey.questions.size
        )
        if (state.survey.questions[viewPager.currentItem]?.required == true) {
          toolbar.setTitle(
              resources.getString(
                  R.string.labels_cma_core_common_mandatory_field,
                  state.survey.questions[viewPager.currentItem]?.question as String
              )
          )

        } else {
          toolbar.setTitle(state.survey.questions[viewPager.currentItem]?.question as String)
          toolbar.setScrollabeTitle(true)
        }
        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
          override fun onPageScrollStateChanged(state: Int) {
          }

          override fun onPageScrolled(
              position: Int,
              positionOffset: Float,
              positionOffsetPixels: Int
          ) {
          }

          override fun onPageSelected(position: Int) {
            nextButton.text = resources.getText(R.string.labels_cma_core_action_next)
            if ((position - 1) >= 0 && state.survey.questions[position - 1]?.required == true && state.surveyRequest.answers[position - 1]?.answerValue ?: "" == "" && !isBack) {
              goToBackPage()
              if (position == 0) {
                backButton.visibility = View.GONE
              } else {
                backButton.visibility = View.VISIBLE
              }
            } else {
              isBack = false
              loader.progress = (viewPager.currentItem + 1)
              questionCounterLabel.text = resources.getString(
                  R.string.labels_cma_content_survey_question_format, (viewPager.currentItem + 1),
                  state.survey.questions.size
              )
              if (state.survey.questions[position]?.required == true) {
                toolbar.setTitle(
                    resources.getString(
                        R.string.labels_cma_core_common_mandatory_field,
                        state.survey.questions[viewPager.currentItem]?.question as String
                    )
                )
              } else {
                toolbar.setTitle(state.survey.questions[viewPager.currentItem]?.question as String)
              }
              toolbar.setScrollabeTitle(true)
              if ((position + 1) == loader.max) {
                nextButton.text = resources.getText(R.string.labels_cma_core_action_send)
                isAnswerCheckedAndRequired(questionList = state.survey.questions,
                    answerList = state.surveyRequest.answers)
                loaderOneQuestion()
              } else {
                backButton.visibility = View.VISIBLE
              }
              if (position == 0) {
                backButton.visibility = View.GONE
              } else {
                backButton.visibility = View.VISIBLE
              }
            }
          }
        })
      }
    } else if (state.survey?.isSurveyDetailsDownloaded == true && state.survey?.questions?.size == 0) {
      backButton.visibility = View.GONE
      nextButton.visibility = View.GONE
      if (!state.isShowDialogEmpty) {
        presenter.showDialogEmptyQuestion()
        state.isShowDialogEmpty = true
      }
    }
  }

  open fun initAdapter(survey: Survey, state: SurveyDetailsViewState) {
    surveyAdapter = SurveyAdapter(context, survey, state.surveyRequest, this)
  }

  protected fun loaderOneQuestion() {
    if (loader.max == 0) {
      loader.visibility = View.INVISIBLE
      loader.max++
      loader.progress = loader.max
    }
  }

  protected fun emptyAnserwList(survey: Survey): RealmList<SurveyAnswer> {
    var list: RealmList<com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyAnswer> =
        RealmList()
    survey.questions.forEach {
      var emptyAnswer = SurveyAnswer(questionId = it.questionId)
      list.add(emptyAnswer)
    }
    return list
  }

  override lateinit var presenter: SurveyDetailsPresenter
  override fun inject(fragment: BaseFragment) {
    super.inject(fragment)
    presenter = injector(context).with(
        Pair<ContentContract.SurveyDetailsView, androidx.fragment.app.Fragment>(
            this as ContentContract.SurveyDetailsView, fragment
        )
    )
        .instance()
    toolbar.setState(BACK)
    toolbar.setCLMBackgroundColorColor(R.string.styles_color_primary)
    toolbar.setHeightRoot(160)
    toolbar.setTitleStyle(HEADER_2_ON_PRIMARY)
  }

  companion object {
    val ENTRY = NavigationPoint(R.layout.screen_survey)
  }
}