package com.comarch.clm.mobileapp.content.survey.data.model.realm

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class ContentRealmModule