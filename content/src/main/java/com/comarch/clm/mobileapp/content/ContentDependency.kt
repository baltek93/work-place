package com.comarch.clm.mobileapp.content

import com.comarch.clm.mobileapp.content.survey.data.RestSurveyRepository
import com.comarch.clm.mobileapp.content.survey.data.SurveyRepository
import com.comarch.clm.mobileapp.content.survey.domain.SurveyUseCase
import com.comarch.clm.mobileapp.content.survey.presentation.SurveyDetailsPresenter
import com.comarch.clm.mobileapp.content.survey.presentation.SurveyDetailsViewModel
import com.comarch.clm.mobileapp.content.survey.presentation.SurveyListPresenter
import com.comarch.clm.mobileapp.content.survey.presentation.SurveyListViewModel
import com.comarch.clm.mobileapp.core.Architecture.CustomView
import com.comarch.clm.mobileapp.core.presentation.BaseFragment
import com.comarch.clm.mobileapp.core.util.ClmDateFormatter
import com.comarch.clm.mobileapp.core.util.ClmDateFormatter.Companion
import com.comarch.clm.mobileapp.core.util.ClmDateFormatter.Companion.APP_DATE_FORMAT
import com.comarch.clm.mobileapp.core.util.viewModelOf
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.factory
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import com.github.salomonbrys.kodein.singleton
import com.github.salomonbrys.kodein.with
import retrofit2.Retrofit

val contentDependency = Kodein.Module {

  bind<ContentContract.SurveyListPresenter>() with factory { dependency: Pair<ContentContract.SurveyListView, androidx.fragment.app.Fragment> ->
    SurveyListPresenter(
        dependency.first, instance(), instance(), with(dependency.second).instance(),
        with(dependency.first as CustomView).instance()
    )
  }

  bind<ContentContract.SurveyListViewModel>() with factory { fragment: androidx.fragment.app.Fragment ->
    viewModelOf(
        fragment, SurveyListViewModel::class.java
    )
  }




  bind<ContentContract.SurveyUseCase>() with provider {
    SurveyUseCase(instance(), instance(), instance(), instance(), instance(),instance())
  }
  bind<ContentContract.SurveyRepository>() with provider {
    SurveyRepository(instance(), instance(), instance())
  }
  bind<RestSurveyRepository>() with provider {
    instance<Retrofit>().create(
        RestSurveyRepository::class.java
    )
  }

  bind<ContentContract.SurveyDetailsPresenter>() with factory { dependency: Pair<ContentContract.SurveyDetailsView, androidx.fragment.app.Fragment> ->
    SurveyDetailsPresenter(
        dependency.first, instance(), instance(), with(dependency.second).instance(),
        with(dependency.first as CustomView).instance()
    )
  }

  bind<ContentContract.SurveyDetailsViewModel>() with factory { fragment: androidx.fragment.app.Fragment ->

    val surveyId = fragment.arguments?.getString(BaseFragment.OBJECT_ID) ?: ""
    viewModelOf(fragment, SurveyDetailsViewModel::class.java, surveyId)

  }


  bind<SurveyListViewModel.Observers.GetSurveysObserver>() with factory { viewModel: SurveyListViewModel -> viewModel.GetSurveyObserver() }
  bind<SurveyListViewModel.Observers.UpdateSurveysObserver>() with provider { SurveyListViewModel.UpdateSurveysObserver() }

  bind<SurveyDetailsViewModel.Observers.GetSurveyObserver>() with factory { viewModel: SurveyDetailsViewModel -> viewModel.GetSurveyObserver() }
  bind<SurveyDetailsViewModel.Observers.UpdateSurveyObserver>() with provider { SurveyDetailsViewModel.UpdateSurveyObserver() }

  bind<ClmDateFormatter>(ContentContract.SURVEY_SEND_DATE_FORMATTER) with singleton {
    ClmDateFormatter(APP_DATE_FORMAT, ClmDateFormatter.CLM_DATE_TIME_FORMAT,
        APP_DATE_FORMAT, ClmDateFormatter.APP_DATE_TIME_FORMAT)
  }

}