package com.comarch.clm.mobileapp.content.survey.data.model

import io.realm.RealmList
import java.util.Date

interface Survey {
  val code: String
  val name: String
  val startDate: Date?
  val endDate: Date?
  var description: String
  var status: String
  var statusName: String
  val questions: RealmList<out SurveyQuestion>
  var filledByUser: Boolean
  val isSurveyDetailsDownloaded: Boolean
  val displayMode: String?

  companion object {
    val FIELD_FILLED_BY_USER = "filledByUser"
    val FIELD_STATUS = "status"
    val STATUS_ACTIVE = "A"
    val FIELD_START_DATE = "startDate"
    val FIELD_END_DATE = "endDate"
    val FIELD_NAME = "name"
  }
}

interface SurveyQuestion {
  val questionId: Long
  val name: String
  val question: String
  val type: String
  val required: Boolean
  val validateRegexp: String
  val attId: Long
  val sflId: Long
  val dispAttValue: Boolean
  val answerChoices: RealmList<out AnswerChoice>
  val displayMode: String?
}

interface AnswerChoice {
  val code: String
  val name: String
  val answer: String
  val sflId: Long
  val id: Long
}

interface SurveyRequest {
  var code: String
  var marketingOfferCode: String
  val answers: RealmList<out SurveyAnswer>
}

interface SurveyAnswer {
  var questionId: Long
  var answerValue: String
}

interface SurveyFilled {
  val answers: RealmList<out SurveyAnswer>
  val fillingDate: String
  val survey: Survey?

}