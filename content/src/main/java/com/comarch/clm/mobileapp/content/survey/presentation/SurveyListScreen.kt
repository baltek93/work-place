package com.comarch.clm.mobileapp.content.survey.presentation

import android.content.Context
import com.google.android.material.appbar.AppBarLayout
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.comarch.clm.mobileapp.content.ContentContract
import com.comarch.clm.mobileapp.content.ContentContract.SurveyListPresenter
import com.comarch.clm.mobileapp.content.R
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.Navigator.NavigationPoint
import com.comarch.clm.mobileapp.core.presentation.BaseFragment
import com.comarch.clm.mobileapp.core.util.ClmDateFormatter
import com.comarch.clm.mobileapp.core.util.components.CLMLabel
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import kotlinx.android.synthetic.main.screen_survey_list.view.appbar
import kotlinx.android.synthetic.main.screen_survey_list.view.surveyList
import kotlinx.android.synthetic.main.screen_survey_list.view.surveysTitle
import kotlinx.android.synthetic.main.screen_survey_list.view.toolbar
import kotlinx.android.synthetic.main.survey_item_list.view.descriptionSurvey
import kotlinx.android.synthetic.main.survey_item_list.view.endDateSurvey
import kotlinx.android.synthetic.main.survey_item_list.view.nameSurvey

class SurveyListScreen @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(
    context, attrs,
    defStyleAttr
), ContentContract.SurveyListView {

  var isClickItem = false
  private val dateFormatter: ClmDateFormatter = injector(context).instance()


  override fun render(state: ContentContract.SurveyListViewState) {
    surveyList.render(object : Architecture.CLMListView.Renderable {
      override val size: Int = state.surveys.size
      override fun bindView(
          view: View,
          position: Int
      ) {
        view.nameSurvey.text = state.surveys[position].name
        view.endDateSurvey.text = dateFormatter.formatDate(
            state.surveys[position].endDate)
        view.descriptionSurvey.text = state.surveys[position].description
        view.setOnClickListener {
          presenter.navigateToSurveys(state.surveys[position])
        }
        if (state.surveys[position].description.isNullOrBlank()) {
          view.descriptionSurvey.visibility = View.GONE
        } else {
          view.descriptionSurvey.visibility = View.VISIBLE
        }

        if (state.surveys[position].endDate==null) {
          view.endDateSurvey.visibility = View.GONE
        } else {
          view.endDateSurvey.visibility = View.VISIBLE
        }
      }

      override fun onItemClick(item: View, position: Int) {
        if (isClickItem) {
          surveyList.isClickable = false
        }
        isClickItem = !isClickItem

      }
    }

    )

  }

  override lateinit var presenter: SurveyListPresenter

  override fun inject(fragment: BaseFragment) {
    super.inject(fragment)
    presenter = injector(context).with(
        Pair<ContentContract.SurveyListView, androidx.fragment.app.Fragment>(
            this as ContentContract.SurveyListView, fragment
        )
    )
        .instance()
  }

  override fun init() {
    super.init()
//    toolbar.hideBackgroundImage()
    initTitle()
    surveyList.setLayout(R.layout.survey_item_list)
    surveyList.isMotionEventSplittingEnabled = false

  }

  private fun initTitle() {
    val mListener = AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
      if (surveysTitle.height + verticalOffset <= 1) {
        surveysTitle.animate().alpha(0f)
        toolbar.findViewById<CLMLabel>(R.id.toolbar_title).animate().alpha(1f)
        toolbar.setTitle(R.string.labels_cma_content_survey_title)
      } else {
        toolbar.findViewById<CLMLabel>(R.id.toolbar_title).animate().alpha(0f)
        toolbar.setTitle("")
        surveysTitle.animate().alpha(1f)
      }
    }
    appbar.addOnOffsetChangedListener(mListener)
  }

  companion object {
    val ENTRY = NavigationPoint(R.layout.screen_survey_list)
  }
}