package com.comarch.clm.mobileapp.content.survey.presentation

import com.comarch.clm.mobileapp.content.ContentContract.SurveyDetailsPresenter
import com.comarch.clm.mobileapp.content.ContentContract.SurveyDetailsView
import com.comarch.clm.mobileapp.content.ContentContract.SurveyDetailsViewModel
import com.comarch.clm.mobileapp.content.ContentContract.SurveyDetailsViewState
import com.comarch.clm.mobileapp.content.R
import com.comarch.clm.mobileapp.content.survey.presentation.EmptyQuestionDialog.Listener
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.UiEventHandler
import com.comarch.clm.mobileapp.core.presentation.BasePresenter

open class SurveyDetailsPresenter(
    private val view: SurveyDetailsView,
    private val router: Architecture.Router,
    private val navigator: Architecture.Navigator<Architecture.Navigator.NavigationScreen>,
    viewModel: SurveyDetailsViewModel,
    uiEventHandler: UiEventHandler
) : BasePresenter<SurveyDetailsViewState, SurveyDetailsViewModel>(
    viewModel, uiEventHandler),
    SurveyDetailsPresenter, Listener {
  override fun backToListSurvey() {
    router.previousScreen()
  }

  override fun showDialogEmptyQuestion() {
    navigator.showCLMDialog(EmptyQuestionDialog.getInstance(listener = this)
    )
  }

  override fun onCheckSurveyCompleted() {
    if (viewModel.checkSurveyCompleted()) {
      onSendPressed()
    }
  }

  override fun onSendPressed() {
    val description = view.getContext().resources?.getString(
        R.string.labels_cma_content_survey_question_completing_survey
    ) ?: ""
    val name =
        view.getContext().resources?.getString(R.string.labels_cma_content_survey_question_thankYou)
            ?: ""
    navigator.showCLMDialog(
        SurveyFinishDialog.getInstance(name = name, description = description, listener = viewModel)
    )

  }

  override fun onViewStateChanged(state: SurveyDetailsViewState) {
    if (state.isFinishSurvey) {
      router.previousScreen()
    } else {
      view.render(state)
    }
  }

  init {
    view.clickNextPressed()
        .subscribe {
          view.goToNextPageOrFinishSurvey()
        }
    view.clickBackPressed()
        .subscribe {
          view.goToBackPage()
        }
  }
}