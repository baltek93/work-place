package com.comarch.clm.mobileapp.content

import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyRequest
import com.comarch.clm.mobileapp.content.survey.presentation.AnswerCheckedListener
import com.comarch.clm.mobileapp.content.survey.presentation.SurveyFinishDialog
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.Screen
import com.comarch.clm.mobileapp.core.data.repository.filter.Predicate
import com.comarch.clm.mobileapp.core.presentation.BaseView
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel
import com.comarch.clm.mobileapp.core.presentation.BaseViewModel.ApplicationViewState
import com.comarch.clm.mobileapp.core.presentation.CLMFilterPredicate
import com.comarch.clm.mobileapp.core.util.ClmOptional
import io.reactivex.Completable
import io.reactivex.Observable

interface ContentContract {
  interface SurveyListView : Screen<SurveyListPresenter>, BaseView {
    fun render(state: SurveyListViewState)
  }

  interface SurveyListPresenter : Architecture.Presenter {
    fun navigateToSurveys(survey: Survey)
  }

  interface SurveyListViewModel : Architecture.ViewModel<SurveyListViewState>

  interface SurveyRepository : Architecture.LocalRepository {
    fun getSurveys(globalPredicate: Predicate? = null): Observable<List<Survey>>

    fun updateSurveys(): Completable

    fun updateSurvey(idSurvey: String): Completable
    fun getSurvey(code: String): Observable<ClmOptional<Survey?>>

    fun sendSurvey(surveyRequest: SurveyRequest): Completable
    fun deleteSurvey(surveyCode: String)
  }

  interface SurveyDetailsView : Screen<SurveyDetailsPresenter>, BaseView, AnswerCheckedListener {
    fun render(state: SurveyDetailsViewState)
    fun clickBackPressed(): Observable<Any>
    fun clickNextPressed(): Observable<Any>
    fun goToNextPageOrFinishSurvey()
    fun goToBackPage()
  }

  interface SurveyDetailsPresenter : Architecture.Presenter {
    fun onSendPressed()
    fun onCheckSurveyCompleted()
    fun showDialogEmptyQuestion()
  }

  interface SurveyDetailsViewModel : Architecture.ViewModel<SurveyDetailsViewState>,
      SurveyFinishDialog.Listener {
    fun checkSurveyCompleted(): Boolean
  }

  interface SurveyUseCase : Architecture.UseCase {
    fun updateSurvey(idSurvey: String): Completable
    fun getSurvey(code: String): Observable<ClmOptional<Survey?>>

    fun sendSurvey(
        surveyRequest: com.comarch.clm.mobileapp.content.survey.data.model.SurveyRequest): Completable

    fun getSurveys(filterPredicate: CLMFilterPredicate? = null): Observable<List<Survey>>

    fun updateSurveys(): Completable
  }

  data class SurveyListViewState(
      val surveys: List<Survey> = emptyList(),
      override val stateNetwork: String? = null,
      override val stateSync: String? = null
  ) : BaseViewModel.ApplicationViewState

  data class SurveyDetailsViewState(
      val survey: Survey? = null,
      var surveyRequest: com.comarch.clm.mobileapp.content.survey.data.model.SurveyRequest = com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyRequest(),
      val isFinishSurvey: Boolean = false,
      var isShowDialogEmpty: Boolean = false,
      override val stateNetwork: String? = null,
      override val stateSync: String? = null
  ) : ApplicationViewState

  companion object {
    const val SURVEY_SEND_DATE_FORMATTER = "SURVEY_SEND_DATE_FORMATTER"

  }

}