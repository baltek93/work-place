package com.comarch.clm.mobileapp.content.survey.data

import com.comarch.clm.mobileapp.content.ContentContract.SurveyRepository
import com.comarch.clm.mobileapp.content.survey.data.model.AnswerChoice
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyFilled
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyQuestion
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyRequest
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.LocalRepository
import com.comarch.clm.mobileapp.core.data.model.Dictionary
import com.comarch.clm.mobileapp.core.data.repository.filter.Predicate
import com.comarch.clm.mobileapp.core.util.ClmOptional
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class SurveyRepository(
    private val localRepository: LocalRepository,
    private val remoteRepository: RestSurveyRepository,
    private val tokenRepositroy: Architecture.TokenRepository

) : SurveyRepository, Architecture.LocalRepository by localRepository {
  override fun deleteSurvey(surveyCode: String) {
    val survey = localRepository.getByCode(Survey::class.java, surveyCode)
    localRepository.delete(survey)
  }

  override fun sendSurvey(surveyRequest: SurveyRequest): Completable {
    return tokenRepositroy.getToken()
        .observeOn(Schedulers.io())
        .flatMapCompletable {
          remoteRepository.filledSurvey(it.token,
              surveyRequest as com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyRequest)
        }
  }

  override fun updateSurvey(idSurvey: String): Completable {
    return tokenRepositroy.getToken()
        .observeOn(Schedulers.io())
        .flatMap {
          remoteRepository.getSurvey(it.token, idSurvey = idSurvey)
              .observeOn(AndroidSchedulers.mainThread())
              .map {
                it.isSurveyDetailsDownloaded = true
                localRepository.save(it)
              }
        }
        .toCompletable()
  }

  override fun getSurvey(
      code: String): Observable<ClmOptional<Survey?>> {
    return localRepository.getObservable(Survey::class.java, Dictionary.CODE, code)
  }

  init {
    localRepository.registerType(
        Survey::class.java,
        com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey::class.java
    )
    localRepository.registerType(
        SurveyQuestion::class.java,
        com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyQuestion::class.java
    )
    localRepository.registerType(
        AnswerChoice::class.java,
        com.comarch.clm.mobileapp.content.survey.data.model.realm.AnswerChoice::class.java
    )

    localRepository.registerType(
        SurveyFilled::class.java,
        com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyFilled::class.java
    )
  }

  override fun updateSurveys(): Completable {
    return tokenRepositroy.getToken()
        .observeOn(Schedulers.io())
        .flatMap {
          Single.zip(
              remoteRepository.getSurveyList(it.token),
              remoteRepository.getFilledSurveyList(it.token),
              BiFunction<List<Survey>, List<SurveyFilled>, List<Survey>> { surveyList, surveyfilledList ->
                surveyfilledList.forEach { surveyFilled ->
                  surveyList.map {
                    if (it.code == surveyFilled.survey?.code ?: "") {
                      it.filledByUser = true
                    }
                  }
                }

                surveyList
              })
              .observeOn(AndroidSchedulers.mainThread())
              .toObservable()
              .flatMapIterable {
                it as List<com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey>
              }
              .map(rewriteDetailsLocalParams())
              .toList()
              .map {
                localRepository.save(it)
              }
        }
        .toCompletable()
  }

  override fun getSurveys(globalPredicate: Predicate?): Observable<List<Survey>> {
    return localRepository.getObservableList(
        Survey::class.java, predicate = globalPredicate)
  }

  private fun rewriteDetailsLocalParams(): (com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey) -> com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey {
    return {
      val survey = localRepository.getCopyByCode(
          com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey::class.java,
          it.code
      )
      if (survey != null) {
        it.questions = survey.questions
        it.description = survey.description
        it.status = survey.status
        it.statusName = survey.statusName
        it.isSurveyDetailsDownloaded = survey.isSurveyDetailsDownloaded
      }
      it
    }
  }
}