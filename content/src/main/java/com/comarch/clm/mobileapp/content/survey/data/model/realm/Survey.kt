package com.comarch.clm.mobileapp.content.survey.data.model.realm

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.Date

open class Survey(
    @PrimaryKey override var code: String = "",
    override var name: String = "",
    override var startDate: Date? = null,
    override var endDate: Date? = null,
    override var description: String = "",
    override var status: String = "",
    override var statusName: String = "",
    override var questions: RealmList<SurveyQuestion> = RealmList(),
    override var filledByUser: Boolean = false,
    override var isSurveyDetailsDownloaded: Boolean = false,
    override var displayMode: String? = null
) : RealmObject(), com.comarch.clm.mobileapp.content.survey.data.model.Survey

open class SurveyQuestion(
    override var questionId: Long = 0,
    override var name: String = "",
    override var question: String = "",
    override var type: String = "",
    override var required: Boolean = false,
    override var validateRegexp: String = "",
    override var attId: Long = 0,
    override var sflId: Long = 0,
    override var dispAttValue: Boolean = false,
    override var answerChoices: RealmList<AnswerChoice> = RealmList(),
    override var displayMode: String? = null
) : RealmObject(), com.comarch.clm.mobileapp.content.survey.data.model.SurveyQuestion

open class AnswerChoice(
    override var code: String = "",
    override var name: String = "",
    override var answer: String = "",
    override var sflId: Long = 0,
    override var id: Long = 0
) : RealmObject(), com.comarch.clm.mobileapp.content.survey.data.model.AnswerChoice

open class SurveyRequest(
    override var code: String = "",
    override var marketingOfferCode: String = "",
    override var answers: RealmList<SurveyAnswer> = RealmList()
) : RealmObject(), com.comarch.clm.mobileapp.content.survey.data.model.SurveyRequest

open class SurveyAnswer(
    override var questionId: Long = 0,
    override var answerValue: String = ""
) : RealmObject(), com.comarch.clm.mobileapp.content.survey.data.model.SurveyAnswer

open class SurveyFilled(
    override var answers: RealmList<SurveyAnswer> = RealmList(),
    override var fillingDate: String = "",
    override var survey: Survey? = null
) : RealmObject(), com.comarch.clm.mobileapp.content.survey.data.model.SurveyFilled
