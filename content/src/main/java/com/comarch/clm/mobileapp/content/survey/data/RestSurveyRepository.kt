package com.comarch.clm.mobileapp.content.survey.data

import com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey
import com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyFilled
import com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyRequest
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface RestSurveyRepository {

  @GET("me/surveys") fun getSurveyList(
    @Header(
        "Authorization"
    ) authorization: String
  ): Single<List<Survey>>

  @GET("me/surveys/{id}") fun getSurvey(
    @Header(
        "Authorization"
    ) authorization: String, @Path("id") idSurvey: String
  ): Single<Survey>

  @GET("me/filled-surveys") fun getFilledSurveyList(
    @Header(
        "Authorization"
    ) authorization: String
  ): Single<List<SurveyFilled>>

  @POST("me/filled-surveys") fun filledSurvey(
    @Header(
        "Authorization"
    ) authorization: String, @Body obj: SurveyRequest
  ): Completable
}