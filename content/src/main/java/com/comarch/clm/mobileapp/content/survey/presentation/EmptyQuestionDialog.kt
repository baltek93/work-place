package com.comarch.clm.mobileapp.content.survey.presentation

import android.content.DialogInterface
import androidx.annotation.StringRes
import android.view.View
import com.comarch.clm.mobileapp.content.R
import com.comarch.clm.mobileapp.core.presentation.dialog.CLMDialog
import kotlinx.android.synthetic.main.dialog_survey.view.dialogSurveyCancelButton
import kotlinx.android.synthetic.main.dialog_survey.view.dialogSurveyDescription
import kotlinx.android.synthetic.main.dialog_survey.view.dialogSurveyName
import kotlinx.android.synthetic.main.dialog_survey.view.dialogSurveyStartButton

class EmptyQuestionDialog : CLMDialog() {
  @StringRes
  override fun dialogCancelText() = 0

  override val isRoundDialog: Boolean=true
  companion object {
    val BUNDLE_SURVEY_NAME = SurveyDialog.javaClass.canonicalName + "NAME"
    val BUNDLE_SURVEY_ID = SurveyDialog.javaClass.canonicalName + "ID"
    val BUNDLE_SURVEY_DESCRIPTION = SurveyDialog.javaClass.canonicalName + "DESCRIPTION"

    fun getInstance(
        listener: Listener
    ): EmptyQuestionDialog {
      return EmptyQuestionDialog().apply {
        this.listener = listener
      }
    }
  }

  override fun onDismiss(dialog: DialogInterface) {
    super.onDismiss(dialog)
    listener?.backToListSurvey()
  }

  var listener: Listener? = null
  lateinit var dialogView: View
  override fun getLayout(): View {

    dialogView = activity?.layoutInflater?.inflate(R.layout.dialog_survey, null)!!
    dialogView.dialogSurveyName.text = context?.resources?.getString(R.string.labels_cma_content_survey_question_empty_survey_title)?: ""
    dialogView.dialogSurveyDescription.text = context?.resources?.getString(R.string.labels_cma_content_survey_question_empty_survey_description)?: ""
    dialogView.dialogSurveyStartButton.text = context?.resources?.getString(R.string.labels_cma_core_action_ok)?: ""
    dialogView.dialogSurveyCancelButton.visibility = View.GONE
    dialogView.dialogSurveyStartButton.setOnClickListener {
      dismiss()
    }
    dialogView.dialogSurveyCancelButton.setOnClickListener {
      dismiss()
    }
    return dialogView
  }

  interface Listener {
    fun backToListSurvey()
  }
}

