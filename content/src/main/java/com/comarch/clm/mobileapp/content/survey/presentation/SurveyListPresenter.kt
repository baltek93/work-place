package com.comarch.clm.mobileapp.content.survey.presentation

import com.comarch.clm.mobileapp.content.ContentContract
import com.comarch.clm.mobileapp.content.ContentContract.SurveyListView
import com.comarch.clm.mobileapp.content.ContentContract.SurveyListViewModel
import com.comarch.clm.mobileapp.content.ContentContract.SurveyListViewState
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.UiEventHandler
import com.comarch.clm.mobileapp.core.presentation.BasePresenter

open class SurveyListPresenter(
    private val view: SurveyListView,
    private val router: Architecture.Router,
    private val navigator: Architecture.Navigator<Architecture.Navigator.NavigationScreen>,
    viewModel: SurveyListViewModel,
    uiEventHandler: UiEventHandler
) : BasePresenter<SurveyListViewState, SurveyListViewModel>(viewModel, uiEventHandler),
    ContentContract.SurveyListPresenter, SurveyDialog.Listener {
  override fun navigateToDetailsSurveys(id: String) {
    router.nextScreen(id)
  }

  override fun navigateToSurveys(survey: Survey) {
    navigator.showCLMDialog(
        SurveyDialog.getInstance(survey.code, survey.name, survey.description, this)
    )
  }

  override fun onViewStateChanged(state: SurveyListViewState) {
    view.render(state)
  }
}