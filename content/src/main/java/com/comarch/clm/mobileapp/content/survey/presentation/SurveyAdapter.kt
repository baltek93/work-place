package com.comarch.clm.mobileapp.content.survey.presentation

import android.R.style
import android.annotation.SuppressLint
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import androidx.viewpager.widget.PagerAdapter
import androidx.core.widget.NestedScrollView
import android.text.InputType
import android.text.TextUtils
import android.util.LayoutDirection
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.LinearLayout.LayoutParams
import android.widget.LinearLayout.SHOW_DIVIDER_BEGINNING
import android.widget.LinearLayout.SHOW_DIVIDER_END
import android.widget.LinearLayout.SHOW_DIVIDER_MIDDLE
import android.widget.NumberPicker
import android.widget.RadioGroup
import androidx.core.view.ViewCompat
import com.comarch.clm.mobileapp.content.ContentContract
import com.comarch.clm.mobileapp.content.R
import com.comarch.clm.mobileapp.content.R.drawable
import com.comarch.clm.mobileapp.content.R.id
import com.comarch.clm.mobileapp.content.R.string
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyAnswer
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyQuestion
import com.comarch.clm.mobileapp.content.survey.data.model.SurveyRequest
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.presentation.CLMDatePickerDialog
import com.comarch.clm.mobileapp.core.presentation.CLMListView
import com.comarch.clm.mobileapp.core.util.ClmDateFormatter
import com.comarch.clm.mobileapp.core.util.ClmLogger
import com.comarch.clm.mobileapp.core.util.components.CLMCheckBox
import com.comarch.clm.mobileapp.core.util.components.CLMCircleRating
import com.comarch.clm.mobileapp.core.util.components.CLMEditText
import com.comarch.clm.mobileapp.core.util.components.CLMRadioButton
import com.comarch.clm.mobileapp.core.util.components.resources.CLMResourcesResolver
import com.comarch.clm.mobileapp.core.util.components.styles.CLMDatePickerDialogStyle
import com.comarch.clm.mobileapp.core.util.components.styles.CLMRadioButtonStyle
import com.comarch.clm.mobileapp.core.util.injector
import com.github.salomonbrys.kodein.instance
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import java.util.ArrayList
import java.util.Calendar

open class SurveyAdapter(
        private val context: Context,
        private val survey: Survey,
        private val surveyRequest: SurveyRequest,
        private val listener: AnswerCheckedListener
) : PagerAdapter() {
    private val resourcesResolver: CLMResourcesResolver = injector(context).instance()
    private val dateFormatter: ClmDateFormatter = injector(context).instance(
            ContentContract.SURVEY_SEND_DATE_FORMATTER)

    protected open val checkboxTextAligment = View.TEXT_ALIGNMENT_VIEW_END

    protected var surveyViews: ArrayList<View> = ArrayList()
    override fun isViewFromObject(
            view: View,
            `object`: Any
    ): Boolean {
        return view == `object`
    }

    fun checkIfIsAnswered() {
        listener.isAnswerCheckedAndRequired(
                survey.questions, surveyRequest.answers
        )
    }

    init {
        survey.questions.forEach { question ->
            surveyViews.add(
                    when (question?.type) {
                      //date type
                      //dateTime type
                      "D", "C" -> {
                        LayoutInflater.from(context)
                                .inflate(R.layout.survey_short_text_item, null)
                      }
                      //list type
                      "L" -> {
                        if (question?.displayMode != "NPS") {
                          LayoutInflater.from(context)
                                  .inflate(R.layout.survey_radio_group, null)
                        } else {
                          LayoutInflater.from(context)
                                  .inflate(R.layout.view_clm_rate, null)
                        }
                      }
                      //integer type
                      "I" -> {
                        LayoutInflater.from(context)
                                .inflate(R.layout.survey_short_text_item, null)
                      }
                      //long text type
                      "T" -> {
                        LayoutInflater.from(context)
                                .inflate(R.layout.survey_short_text_item, null)
                      }
                      //number type
                      "R" -> {
                        LayoutInflater.from(context)
                                .inflate(R.layout.survey_short_text_item, null)
                      }
                      //boolean
                      "B" -> {
                        LayoutInflater.from(context)
                                .inflate(R.layout.survey_radio_group, null)
                      }
                      //multichoice
                      "M" -> {
                        LayoutInflater.from(context)
                                .inflate(R.layout.survey_simple_question_list, null)
                      }
                      //text short type
                      "S" -> {
                        LayoutInflater.from(context)
                                .inflate(R.layout.survey_short_text_item, null)
                      }
//            "TEST" -> {
//              LayoutInflater.from(context)
//                  .inflate(R.layout.view_clm_rate)
//            }

                      else -> {
                        View(context)
                      }
                    }
            )

        }
    }

    override fun getItemPosition(`object`: Any): Int {
        val item = surveyViews.indexOf(`object`)
        return if (item == -1) {
            -100
        } else {
            item
        }
    }


    @SuppressLint("ResourceType")
    override fun instantiateItem(
            container: ViewGroup,
            position: Int
    ): Any {


        val view = surveyViews[position]
        val question = survey.questions[position]
        val answer = surveyRequest.answers[position]
        listener.isAnswerCheckedAndRequired(
                survey.questions, surveyRequest.answers
        )

        when (question?.type) {
            //data type
          "D" -> {
            logicDateQuestion(question, survey.questions, view, answer, surveyRequest.answers)
          }
            //list type
          "L" -> {
            if (question?.displayMode != "NPS")
              logicListQuestion(question, survey.questions, answer, view, surveyRequest.answers)
            else
              logicRateQuestion(question, survey.questions, answer, view, surveyRequest.answers)
          }
            //integer type
          "I" -> {
            logicIntegerQuestion(question, survey.questions, view, answer, surveyRequest.answers)
          }
            //long text type
          "T" -> {
            logicLongQuestion(question, survey.questions, view, answer, surveyRequest.answers)
          }
            //number type
          "R" -> {
            logicNumberQuestion(question, survey.questions, view, answer, surveyRequest.answers)
          }
            //boolean
          "B" -> {
            logicBooleanQuestion(question, survey.questions, answer, view, surveyRequest.answers)
          }
            //multichoice
          "M" -> {
            logicMultiChoiceQuestion(question, survey.questions, view, answer, surveyRequest.answers)
          }
            //text short type
          "S" -> {
            logicShortTextQuestion(question, survey.questions, view, answer, surveyRequest.answers)
          }
            // date time
          "C" -> {
            logicDateTimeQuestion(question, survey.questions, view, answer, surveyRequest.answers)
          }
        }
        container.addView(view)
        return view
    }

    fun logicDateTimeQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            view: View,
            answer: SurveyAnswer?,
            answerList: List<SurveyAnswer>
    ) {
        val editText = view.findViewById<CLMEditText>(R.id.editText)
        editText.renderHint(context.resources.getString(R.string.labels_cma_content_survey_chooseDate))
        editText.editInputText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_calendar, 0)
        val context = ContextThemeWrapper(context, style.Theme_Holo_Light_Dialog)
        val mCalendar = Calendar.getInstance()
        editText.setInputType(InputType.TYPE_NULL)
        if (answer?.answerValue ?: "" == "") {
            editText.renderText(answer!!.answerValue)
        }
        listener.isAnswerCheckedAndRequired(questionList, answerList)

        val timeDialog: TimePickerDialog
        val timeListener: TimePickerDialog.OnTimeSetListener

        timeListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            mCalendar.set(Calendar.HOUR, hour)
            mCalendar.set(Calendar.MINUTE, minute)
            mCalendar.set(Calendar.SECOND, 0)
            editText.renderText(dateFormatter.parseAndFormatDateTime((mCalendar.timeInMillis)))
            answer?.answerValue = dateFormatter.parseAndSerializeDateTime(
                    dateLong = mCalendar.timeInMillis)
//      isoFormat.format(mCalendar.time)
            editText.renderText(dateFormatter.parseAndFormatDateTime(
                    dateLong = mCalendar.timeInMillis))
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
        timeDialog = TimePickerDialog(
                context, timeListener,
                mCalendar.get(Calendar.HOUR), mCalendar.get(Calendar.MINUTE), true
        )
        timeDialog.window?.setBackgroundDrawable(context.resources.getDrawable(
                com.comarch.clm.mobileapp.core.R.drawable.dialog_rounded))

        val date: OnDateSetListener
        val dialogCLM: CLMDatePickerDialog
        date = OnDateSetListener { view, year, month, dayOfMonth ->
            mCalendar.set(Calendar.YEAR, year)
            mCalendar.set(Calendar.MONTH, month)
            mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            timeDialog.show()
//      answer?.answerValue = dateFormatter.parseAndFormatDate(
//          dateLong = mCalendar.timeInMillis)
////      isoFormat.format(mCalendar.time)
//      editText.renderText(dateFormatter.parseAndFormatDate(
//          dateLong = mCalendar.timeInMillis))
//      listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
        dialogCLM = CLMDatePickerDialog(
                context, R.style.CLMDateDialog, date,
                mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH)
        )
        dialogCLM.datePicker.calendarViewShown = false
        dialogCLM.setStyle(CLMDatePickerDialogStyle.PRIMARY)
        dialogCLM.setAllDividerColor(R.string.styles_color_transparent)
        editText.isFocusableInTouchMode = false
        editText.editInputText.setOnClickListener { dialogCLM.show() }
    }

    protected fun logicDateQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            view: View,
            answer: SurveyAnswer?,
            answerList: List<SurveyAnswer>
    ) {

        val editText = view.findViewById<CLMEditText>(R.id.editText)
        editText.renderHint(context.resources.getString(R.string.labels_cma_content_survey_chooseDate))
        editText.editInputText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_calendar, 0)
        val context = ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog)
//    val isoFormat = SimpleDateFormat(DATE_FORMAT_1.type)
        val mCalendar = Calendar.getInstance()
        editText.setInputType(InputType.TYPE_NULL)
        if (answer?.answerValue ?: "" == "") {
            editText.renderText(answer!!.answerValue)
        }
        listener.isAnswerCheckedAndRequired(questionList, answerList)
        val date: OnDateSetListener
        val dialogCLM: CLMDatePickerDialog
        date = OnDateSetListener { view, year, month, dayOfMonth ->
            mCalendar.set(Calendar.YEAR, year)
            mCalendar.set(Calendar.MONTH, month)
            mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            answer?.answerValue = dateFormatter.parseAndSerializeDate(
                    dateLong = mCalendar.timeInMillis)
//      isoFormat.format(mCalendar.time)
            editText.renderText(dateFormatter.parseAndFormatDate(
                    dateLong = mCalendar.timeInMillis))
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
        dialogCLM = CLMDatePickerDialog(
                context, R.style.CLMDateDialog, date,
                mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH)
        )
        dialogCLM.datePicker.calendarViewShown = false
        dialogCLM.setStyle(CLMDatePickerDialogStyle.PRIMARY)
        dialogCLM.setAllDividerColor(R.string.styles_color_transparent)
        editText.isFocusableInTouchMode = false
        editText.editInputText.setOnClickListener { dialogCLM.show() }
    }

    @SuppressLint("WrongConstant")
    fun logicListQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            answer: SurveyAnswer?,
            view: View,
            answerList: List<SurveyAnswer>
    ) {
        val radioGroup = RadioGroup(context)
        radioGroup.orientation = LinearLayout.VERTICAL
        radioGroup.dividerDrawable = context.getDrawable(drawable.divider_horizontal)
        radioGroup.showDividers = SHOW_DIVIDER_MIDDLE or SHOW_DIVIDER_END or
                SHOW_DIVIDER_BEGINNING
        val layoutParams = LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        val radioButtonLayoutParams = LayoutParams(LayoutParams.MATCH_PARENT,
                CLMResourcesResolver.convertDpToPx(70, context))

        var margin = CLMResourcesResolver.convertDpToPx(16, context)
        var marginTopAndBottom = CLMResourcesResolver.convertDpToPx(16, context)
        radioButtonLayoutParams.setMargins(margin, marginTopAndBottom, margin, marginTopAndBottom)
        layoutParams.setMargins(0, marginTopAndBottom, 0, marginTopAndBottom)

        for (i in 0 until question.answerChoices.size) {
            val clmRadioButton = CLMRadioButton(context)
            clmRadioButton.setStyle(CLMRadioButtonStyle.SECONDARY)
            clmRadioButton.radioButton.layoutDirection = LayoutDirection.RTL
            clmRadioButton.radioButton.layoutParams = radioButtonLayoutParams

            clmRadioButton.radioButton.id = (1000 + i)
            clmRadioButton.radioButton.text = question.answerChoices[i]?.answer
            clmRadioButton.setRadioButtonTextAligment(View.TEXT_ALIGNMENT_VIEW_END)
            if (clmRadioButton.radioButton.parent != null) {
                (clmRadioButton.radioButton.parent as ViewGroup).removeView(clmRadioButton.radioButton)
            }
            radioGroup.addView(clmRadioButton.radioButton)
        }
        question.answerChoices.forEachIndexed { index, answerChoice ->
            if (answerChoice.code == answer?.answerValue) {
                radioGroup.check(1000 + index)
            }
        }
        view.findViewById<NestedScrollView>(R.id.radioContent)
                .removeAllViews()
        view.findViewById<NestedScrollView>(R.id.radioContent)
                .addView(radioGroup, layoutParams)
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            answer?.answerValue = question.answerChoices[checkedId - 1000]?.code ?: ""
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
    }

    protected fun logicIntegerQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            view: View,
            answer: SurveyAnswer?,
            answerList: List<SurveyAnswer>
    ) {
        val editText = view.findViewById<CLMEditText>(id.editText)
        editText.renderHint(context.getString(string.labels_cma_content_survey_enterTheNumber))
        editText.setInputType(InputType.TYPE_CLASS_NUMBER)
        inputText(editText).subscribe {
            answer?.answerValue = it.toString()
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
    }

    protected fun logicLongQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            view: View,
            answer: SurveyAnswer?,
            answerList: List<SurveyAnswer>
    ) {
        val editText = view.findViewById<CLMEditText>(R.id.editText)
        editText.renderHint(context.getString(string.labels_cma_content_survey_enterTheText))
        editText.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE)
        editText.setMaxLine(6)
        inputText(editText).subscribe {
            answer?.answerValue = it.toString()
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
    }

    protected fun logicNumberQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            view: View,
            answer: SurveyAnswer?,
            answerList: List<SurveyAnswer>
    ) {
        val editText = view.findViewById<CLMEditText>(id.editText)
        editText.renderHint(context.getString(string.labels_cma_content_survey_enterTheNumber))
        editText.setInputType(InputType.TYPE_CLASS_NUMBER)
        editText.requestFocus()

        inputText(editText).subscribe {
            answer?.answerValue = it.toString()
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
    }

    protected fun logicShortTextQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            view: View,
            answer: SurveyAnswer?,
            answerList: List<SurveyAnswer>
    ) {
        val editText = view.findViewById<CLMEditText>(id.editText)
        editText.renderHint(
                context.resources.getString(string.labels_cma_content_survey_enterTheText)
        )
        inputText(editText).subscribe {
            answer?.answerValue = it.toString()
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
    }


    protected fun logicRateQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            answer: SurveyAnswer?,
            view: View,
            answerList: List<SurveyAnswer>
    ) {
        val circleRating = view.findViewById<CLMCircleRating>(
                R.id.circleRating)



        circleRating.rateChanged().subscribe { answerGiven ->
            question.answerChoices.forEach {
                if (it.answer == answerGiven) {
                    answer?.answerValue = it.id.toString()
                    return@forEach
                }
            }
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }


//    val circleRating = view.findViewById<CLMCircleRating>(
//        com.comarch.clm.mobileapp.core.R.id.circle)
    }

    open fun logicMultiChoiceQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            view: View,
            answer: SurveyAnswer?,
            answerList: List<SurveyAnswer>
    ) {
        val surveyQuestionList = view.findViewById<CLMListView>(R.id.surveyQuestionList)
        surveyQuestionList.setLayout(R.layout.survey_multichoice_item)
        surveyQuestionList.render(
                CheckBoxesRenderable(
                        question.answerChoices.size, question, questionList, answer as SurveyAnswer, listener,
                        answerList
                )
        )
    }

    @SuppressLint("ResourceType")
    protected fun logicBooleanQuestion(
            question: SurveyQuestion,
            questionList: List<SurveyQuestion>,
            answer: SurveyAnswer?,
            view: View,
            answerList: List<SurveyAnswer>
    ) {
        val radioGroup = RadioGroup(context)
        radioGroup.orientation = LinearLayout.VERTICAL
        radioGroup.dividerDrawable = context.getDrawable(drawable.divider_horizontal)
        radioGroup.showDividers = SHOW_DIVIDER_MIDDLE or SHOW_DIVIDER_END or
                SHOW_DIVIDER_BEGINNING
        val layoutParams = LayoutParams(
                LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT
        )
        val radioButtonLayoutParams = LayoutParams(
                LayoutParams.MATCH_PARENT, CLMResourcesResolver.convertDpToPx(70, context)
        )
        var margin = CLMResourcesResolver.convertDpToPx(16, context)
        var marginTopAndBottom = CLMResourcesResolver.convertDpToPx(16, context)
        radioButtonLayoutParams.setMargins(margin, marginTopAndBottom, margin, marginTopAndBottom)
        for (i in 0..1) {
            val clmRadioButton = CLMRadioButton(context)
            clmRadioButton.setStyle(CLMRadioButtonStyle.SECONDARY)
            if (TextUtils.getLayoutDirectionFromLocale(context.resources.configuration.locale) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                clmRadioButton.radioButton.layoutDirection = LayoutDirection.LTR
            }
            else {
                clmRadioButton.radioButton.layoutDirection = LayoutDirection.RTL
            }
            clmRadioButton.radioButton.layoutParams = radioButtonLayoutParams
            clmRadioButton.radioButton.setPadding(margin, 0, margin, 0)
            clmRadioButton.radioButton.id = (1000 + i)
            if (i == 0) {
                clmRadioButton.radioButton.text =
                        view.context.resources.getString(R.string.labels_cma_core_action_yes)
            } else {
                clmRadioButton.radioButton.text =
                        view.context.resources.getString(R.string.labels_cma_core_action_no)
            }
            if (clmRadioButton.radioButton.parent != null) {
                (clmRadioButton.radioButton.parent as ViewGroup).removeView(clmRadioButton.radioButton)
            }
            radioGroup.addView(clmRadioButton.radioButton)
        }

        when (answer?.answerValue) {
          "1" -> radioGroup.check(1000)
          "0" -> radioGroup.check(1001)
        }
        view.findViewById<NestedScrollView>(id.radioContent)
                .removeAllViews()
        view.findViewById<NestedScrollView>(id.radioContent)
                .addView(radioGroup, layoutParams)
        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
              1000 -> answer?.answerValue = "1"
              1001 -> answer?.answerValue = "0"
            }
            listener.isAnswerCheckedAndRequired(questionList, answerList)
        }
    }

    override fun destroyItem(
            container: ViewGroup,
            position: Int,
            `object`: Any
    ) {
        container.removeView(surveyViews[position])
    }

    override fun getCount(): Int = survey.questions.size

    private fun setDividerColor(picker: NumberPicker) {
        if (picker == null) return
        val count = picker.childCount
        for (i in 0..count) {
            val dividerField = picker.javaClass.getDeclaredField("mSelectionDivider")
            dividerField.isAccessible = true
            var colorDrawable =
                    ColorDrawable(
                            resourcesResolver.resolveColor(
                                    com.comarch.clm.mobileapp.core.R.string.styles_color_surface
                            )
                    )
            dividerField.set(picker, colorDrawable)
            picker.invalidate()
        }
    }

    inner class CheckBoxesRenderable(
            override var size: Int,
            private val question: SurveyQuestion,
            private val questionList: List<SurveyQuestion>,
            private val answer: SurveyAnswer,
            protected val listener: AnswerCheckedListener,
            private val answerList: List<SurveyAnswer>
    ) : Architecture.CLMListView.Renderable {
        override fun bindView(
                view: View,
                position: Int
        ) {
            listener.isAnswerCheckedAndRequired(questionList, answerList)
            view.findViewById<CLMCheckBox>(R.id.checkbox).setCheckboxTextAligment(
                    checkboxTextAligment)

            view.findViewById<CLMCheckBox>(R.id.checkbox)
                    .checkbox.text = question.answerChoices[position]?.answer
            view.findViewById<CLMCheckBox>(R.id.checkbox)
                    .checkbox
            if (answer.answerValue != "") {
                var array = answer.answerValue.split(",")
                array.forEach {
                    if (it.toLong() == question.answerChoices[position]?.id) {
                        view.findViewById<CLMCheckBox>(R.id.checkbox)
                                .checkbox.isChecked = true
                    }
                }
            }
            view.findViewById<CLMCheckBox>(R.id.checkbox)
                    .checkbox
                    .setOnClickListener {
                        if ((it as CheckBox).isChecked) {
                            if (answer.answerValue != "") {
                                answer.answerValue += "," + question.answerChoices[position]?.id
                            } else {
                                answer.answerValue = question.answerChoices[position]?.id.toString()
                                        ?: ""
                            }
                        } else {
                            if (answer.answerValue != "") {
                                var array = answer.answerValue.split(",")
                                        .toMutableList()
                                var iterator = answer.answerValue.split(",")
                                        .iterator()

                                while (iterator.hasNext()) {
                                    val it = iterator.next()
                                    if (it.toLong() == question.answerChoices[position]?.id) {
                                        array.remove(it)
                                    }
                                }
                                answer.answerValue = ""
                                array.forEachIndexed { index, s ->
                                    if (index == (array.size - 1)) {
                                        answer.answerValue += s
                                    } else {
                                        answer.answerValue += "$s,"
                                    }
                                }
                            }
                        }
                        listener.isAnswerCheckedAndRequired(questionList, answerList)
                    }
        }
    }

    private fun inputText(textView: CLMEditText): Observable<String> {
        return RxTextView.textChanges(textView.editInputText)
                .map {
                    it.toString()
                }
    }
}

interface AnswerCheckedListener {
    fun isAnswerCheckedAndRequired(
            questionList: List<SurveyQuestion>,
            answerList: List<SurveyAnswer>
    )
}