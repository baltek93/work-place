package com.comarch.clm.mobileapp.content

import com.comarch.clm.mobileapp.content.survey.data.RestSurveyRepository
import com.comarch.clm.mobileapp.content.survey.data.SurveyRepository
import com.comarch.clm.mobileapp.content.survey.data.model.realm.AnswerChoice
import com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey
import com.comarch.clm.mobileapp.content.survey.data.model.realm.SurveyQuestion
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.TokenRepository
import com.comarch.clm.mobileapp.core.data.model.TokenModel
import com.comarch.clm.mobileapp.core.util.ClmOptional
import com.nhaarman.mockitokotlin2.any
import io.kotlintest.specs.AbstractBehaviorSpec
import io.kotlintest.specs.BehaviorSpec
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.realm.RealmList
import org.assertj.core.api.Assertions
import org.mockito.Mockito
import resetSchedulers
import setUpSchedulers
import java.util.Date

class SurveyDataTest : BehaviorSpec({
  setUpSchedulers()
  repositoryForListTest()
//  repositoryForDetailsTest()
  resetSchedulers()

})

val survey = Survey(
    name = "Survey1", code = "1", startDate = Date(1, 1, 2017), endDate = Date(1, 1, 2019),
    description = "description", filledByUser = false, questions = RealmList(
    SurveyQuestion(
        questionId = 1001, name = "MultiChoice", question = "Multichoice question", type = "M",
        required = true, answerChoices = RealmList(

        AnswerChoice(code = "A_1", name = "variant one ", answer = "one"),
        AnswerChoice(code = "A_2", name = "variant one ", answer = "two")
    )
    ), SurveyQuestion(
    questionId = 1001, name = "LongText", question = "Text", type = "T", required = false
)
)
)
val surveyList =
    listOf(
        survey,
        Survey(name = "Survey2", code = "2", startDate = Date(1, 1, 2017),
            endDate = Date(1, 1, 2018))

    )

val observableList = Observable.just(
    surveyList as List<com.comarch.clm.mobileapp.content.survey.data.model.Survey>
)


val singleList = Single.just(surveyList)


private fun AbstractBehaviorSpec.repositoryForListTest() {
  val (localRepository, remoteRepository, tokenRepository) = getMockRepositories()
  val (surveyList, observableList) = setUpReturnValuesForList(
      localRepository, remoteRepository,
      tokenRepository
  )
  val (surveySingle, observableSurvey) = setUpReturnValuesForDetails(
      localRepository, remoteRepository,
      tokenRepository
  )

  val observerSurveyList =
      TestObserver<List<com.comarch.clm.mobileapp.content.survey.data.model.Survey>>()

  Given("SurveyRepository")
  {
    val repository = SurveyRepository(
        localRepository, remoteRepository, tokenRepository
    )
    When("get surveys")
    {
      val surveys = repository.getSurveys()
      Then("call local repository get survey")
      {
        Mockito.verify(localRepository)
            .getObservableList(
                com.comarch.clm.mobileapp.content.survey.data.model.Survey::class.java
            )
        Assertions.assertThat(surveys).isEqualTo(observableList)
      }
    }
    When("get survey")
    {
      val survey = repository.getSurvey("1")
      Then("call local repository get survey")
      {
        Mockito.verify(localRepository)
            .getObservable(com.comarch.clm.mobileapp.content.survey.data.model.Survey::class.java,
                "code", "1")
//        Assertions.assertThat(survey).isEqualTo(observableSurvey)
      }
    }
    When("update surveyList") {
      repository.updateSurveys()
          .subscribe()
      Then("call remote repository get surveys") {
        Mockito.verify(remoteRepository)
            .getSurveyList(any())
      }
    }
  }

}

private fun AbstractBehaviorSpec.repositoryForDetailsTest() {
  val (localRepository, remoteRepository, tokenRepository) = getMockRepositories()
  val (survey, observableList) = setUpReturnValuesForDetails(
      localRepository, remoteRepository,
      tokenRepository
  )

  val observerSurvey =
      TestObserver<com.comarch.clm.mobileapp.content.survey.data.model.Survey>()

  Given("SurveyRepository")
  {
    val repository = SurveyRepository(
        localRepository, remoteRepository, tokenRepository
    )
    When("get surveys")
    {
      val surveys = repository.getSurvey("1")
      Then("call local repository get survey")
      {
        Mockito.verify(localRepository)
            .getObservable(
                com.comarch.clm.mobileapp.content.survey.data.model.Survey::class.java, "code", "1")
        Assertions.assertThat(surveys).isEqualTo(observableList)
      }
    }

    When("update surveyList") {
      repository.updateSurvey("1")
          .subscribe()
      Then("call remote repository get surveys") {
        Mockito.verify(remoteRepository)
            .getSurvey(any(), any())
      }
    }
  }

}

private fun getMockRepositories(): Triple<Architecture.LocalRepository, RestSurveyRepository, TokenRepository> {
  val localRepository = Mockito.mock(Architecture.LocalRepository::class.java)
  val remoteRepository = Mockito.mock(
      RestSurveyRepository::class.java
  )
  val tokenRepository = Mockito.mock(TokenRepository::class.java)
  return Triple(localRepository, remoteRepository, tokenRepository)
}

private fun setUpReturnValuesForList(
    localRepository: Architecture.LocalRepository,
    remoteRepository: RestSurveyRepository,
    tokenRepository: TokenRepository
): Pair<List<com.comarch.clm.mobileapp.content.survey.data.model.Survey>, Observable<List<com.comarch.clm.mobileapp.content.survey.data.model.Survey>>> {

//  var filterPredicate = CLMFilterPredicate()
//  filterPredicate.typesFilterView = arrayListOf(
//      FilterDataView(
//          nameField = "filledByUser", isActive = true, isChecked = true,
//          typesFilter = arrayListOf(PredicateFilterDataSingle(true, defaultValueBoolean = false))
//      )
//  )
  Mockito.`when`(
      localRepository.getObservableList(type =
      com.comarch.clm.mobileapp.content.survey.data.model.Survey::class.java
      )
  )
      .thenReturn(observableList)

  Mockito.`when`(remoteRepository.getSurvey(any(), any())).thenReturn(Single.just(survey))
  Mockito.`when`(remoteRepository.getSurveyList(any()))
      .thenReturn(singleList)
  Mockito.`when`(tokenRepository.getToken())
      .thenReturn(Single.just(TokenModel("e")))
  return Pair(surveyList, observableList)
}

private fun setUpReturnValuesForDetails(
    localRepository: Architecture.LocalRepository,
    remoteRepository: RestSurveyRepository,
    tokenRepository: TokenRepository
): Pair<Survey, Observable<ClmOptional<com.comarch.clm.mobileapp.content.survey.data.model.Survey?>>> {

//  var filterPredicate = CLMFilterPredicate()
//  filterPredicate.typesFilterView = arrayListOf(
//      FilterDataView(
//          nameField = "filledByUser", isActive = true,
//          typesFilter = arrayListOf(PredicateFilterDataSingle(true, defaultValueBoolean = false))
//      )
//  )
  Mockito.`when`(
      localRepository.getObservableList(type =
      com.comarch.clm.mobileapp.content.survey.data.model.Survey::class.java
      )
  )
      .thenReturn(observableList)
  Mockito.`when`(
      localRepository.getObservable(
          type = com.comarch.clm.mobileapp.content.survey.data.model.Survey::class.java,
          fieldName = "code", fieldValue = "1")).thenReturn(Observable.just(ClmOptional.of(survey as com.comarch.clm.mobileapp.content.survey.data.model.Survey?)))
  Mockito.`when`(remoteRepository.getSurvey(any(), any())).thenReturn(Single.just(survey))
  Mockito.`when`(tokenRepository.getToken())
      .thenReturn(Single.just(TokenModel("e")))
  return Pair(survey, Observable.just(
      ClmOptional.of(survey as com.comarch.clm.mobileapp.content.survey.data.model.Survey)))
}