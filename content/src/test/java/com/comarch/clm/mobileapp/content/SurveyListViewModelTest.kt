package com.comarch.clm.mobileapp.content

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.content.survey.presentation.SurveyListViewModel
import com.comarch.clm.mobileapp.core.ApplicationContract
import com.comarch.clm.mobileapp.core.presentation.BaseApplication
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.factory
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.provider
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.assertj.core.api.Assertions
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import java.util.concurrent.TimeUnit.SECONDS

class SurveyListViewModelTest {

  @Rule
  @JvmField
  val instantExecutorRuler = InstantTaskExecutorRule()

  @Test
  fun `view model init test`() {

    val useCase = Mockito.mock(ContentContract.SurveyUseCase::class.java)
    val app: BaseApplication = Mockito.mock(BaseApplication::class.java)
    val surveyList = listOf(
        com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey("1", "1"),
        com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey("2", "2")
    )
    val testObserver = TestObserver.create<Boolean>()

    Mockito.`when`(app.kodein)
        .thenReturn(Kodein {
          bind<ContentContract.SurveyUseCase>() with instance(useCase)

          bind<SurveyListViewModel.Observers.GetSurveysObserver>() with factory { viewModel: SurveyListViewModel -> viewModel.GetSurveyObserver() }
          bind<SurveyListViewModel.Observers.UpdateSurveysObserver>() with provider { SurveyListViewModel.UpdateSurveysObserver() }
        })

    Mockito.`when`(useCase.getSurveys())
        .thenReturn(Observable.just(surveyList as List<Survey>))

    Mockito.`when`(useCase.updateSurveys())
        .thenReturn(Completable.timer(15, SECONDS))

    Mockito.`when`(useCase.getNetworkState())
        .thenReturn(
            Observable.just(ApplicationContract.ApplicationState.NetworkState.CONNECTED)
        )
    Mockito.`when`(useCase.getSyncState())
        .thenReturn(
            Observable.just(ApplicationContract.ApplicationState.SyncState.DONE_SYNCING)
        )

    val viewModel = SurveyListViewModel(
        app
    )

    Mockito.verify(useCase)
        .updateSurveys()
    Mockito.verify(useCase)
        .getSurveys()
    Assertions.assertThat(viewModel.getState().surveys)
        .isEqualTo(surveyList)
    testObserver.hasSubscription()
  }
}