package com.comarch.clm.mobileapp.content

import com.comarch.clm.mobileapp.content.ContentContract.SurveyRepository
import com.comarch.clm.mobileapp.content.survey.data.model.Survey
import com.comarch.clm.mobileapp.content.survey.domain.SurveyUseCase
import com.comarch.clm.mobileapp.core.ApplicationContract
import com.comarch.clm.mobileapp.core.Architecture
import com.comarch.clm.mobileapp.core.Architecture.ErrorHandler
import com.comarch.clm.mobileapp.core.DataSynchronizationContract
import com.comarch.clm.mobileapp.core.data.repository.filter.PredicateFactory
import com.comarch.clm.mobileapp.testutil.getTestApplier
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.kotlintest.specs.AbstractBehaviorSpec
import io.kotlintest.specs.BehaviorSpec
import io.reactivex.Completable
import io.reactivex.CompletableTransformer
import io.reactivex.Observable
import org.assertj.core.api.Assertions
import org.mockito.Mockito
import resetSchedulers
import setUpSchedulers
import java.util.Date

class SurveyDomainTest : BehaviorSpec({
  setUpSchedulers()
  useCaseTest()
  resetSchedulers()
})

fun AbstractBehaviorSpec.useCaseTest() {
  val schedulerApplier = Mockito.mock(Architecture.SchedulerApplier::class.java)
  Mockito.`when`(schedulerApplier.startOnBackgroundCompletable())
      .thenReturn(
          getTestApplier()
      )
  val applicationState = Mockito.mock(ApplicationContract.ApplicationState::class.java)
  Mockito.`when`(applicationState.handleOnSyncCompletable())
      .thenReturn(getTestApplier())
  val (repository, observableList) = getUseCaseDependencies()
  val errorHandler = Mockito.mock(ErrorHandler::class.java)
  val dataSyncManager = Mockito.mock(
      DataSynchronizationContract.DataSynchronizationManager::class.java)
  val predicateFactory = Mockito.mock(PredicateFactory::class.java)
  whenever(errorHandler.handleErrorOnCompletable()).thenReturn(CompletableTransformer { it })
  Given("useCase")
  {
    val useCase = SurveyUseCase(
        errorHandler, schedulerApplier, applicationState,
        repository, dataSyncManager, predicateFactory)
    When("get Survey List")
    {
      val list = useCase.getSurveys()
      Then("should return survey list")
      {
        Assertions.assertThat(list.blockingFirst())
            .isEqualTo(observableList)
      }
    }
  }

}

private fun getUseCaseDependencies(): Pair<SurveyRepository, List<Survey>> {
  val repository = mock<ContentContract.SurveyRepository>()
  val surveyList = listOf<Survey>(
      com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey(
          code = "question1", startDate = Date(10, 12, 2016), description = "description1"
      ),
      com.comarch.clm.mobileapp.content.survey.data.model.realm.Survey(
          code = "question2", startDate = Date(10, 12, 2017), description = "description2"
      )
  )
  val observableList = Observable.just(surveyList)
  val errorHandler = Mockito.mock(Architecture.ErrorHandler::class.java)
  Mockito.`when`(errorHandler.handleErrorOnCompletable())
      .thenReturn(CompletableTransformer { it })
  Mockito.`when`(repository.getSurveys())
      .thenReturn(observableList)
  Mockito.`when`(repository.updateSurveys())
      .thenReturn(Completable.complete())

  return Pair(repository, surveyList)
}